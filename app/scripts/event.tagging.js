'use strict';
/*global jQuery*/
var eventTagging = (function ($) {

  var scrollElements = null;
  var $homepageShelfItems = $('#home .hero-set, #home .intro-section, #home .quotes-list, #home .services-content');
  var $servicesShelfItems = $('#services .international-payments, #services .currency-exchange, #services .affiliates, #services .personal-services');

  //var $servicesSubcategoryItems = $('#services .service-item');

  // Select elements to track in view state and add them into an array.
  function getScrollElements() {

    // Check if elements have been selected so that it's only selected once.
    if (scrollElements) {
      return scrollElements;
    } else {
      scrollElements = [];
    }

    // Keep adding items to the scrollElements array to track items
    var homepageShelfItems = utils.setAsArray($homepageShelfItems);
    scrollElements = scrollElements.concat(homepageShelfItems);

    var servicesShelfItems = utils.setAsArray($servicesShelfItems);
    scrollElements = scrollElements.concat(servicesShelfItems);

    return scrollElements;
  }

  function init() {

    // Navigation tracking
    $('.our-services a').click(function () {
      ga('send', 'event', 'Top Navigation', 'click', 'Our Services');
    });

    $('.our-company a').click(function () {
      ga('send', 'event', 'Top Navigation', 'click', 'Our Company');
    });

    $('.blog a').click(function () {
      ga('send', 'event', 'Top Navigation', 'click', 'FIRMA Blog');
    });

    // Homepage item scroll tracking
    $homepageShelfItems.on('inview', function(e) {
      //var shelf = $(e.currentTarget).find('h3').text();
      ga('send', 'event', 'Homepage', 'Scroll', 'Shelf ' + ($homepageShelfItems.index(this) + 1));
      //console.log($homepageShelfItems.index(this));
    });

    // services item scroll tracking
    $servicesShelfItems.on('inview', function(e) {
      //var shelf = $(e.currentTarget).find('h3').text();
      ga('send', 'event', 'Our Services Page', 'Scroll', 'Shelf ' + ($servicesShelfItems.index(this) + 1));

    });


    $('.dropdown-menu a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var name = $currentTarget.text();
      //console.log(name);
      ga('send', 'event', 'Top Navigation', name, 'Language Dropdown');
    });

    // Homepage

    // Shelf 1,2,3,4

    // Watch video button
    $('.play-trailer-button').click(function () {
      ga('send', 'event', 'Homepage', 'Play YouTube Video', 'Watch how');
    });
    $('.explore .link').click(function () {
      ga('send', 'event', 'Homepage', 'Redirect to services page', 'Explore all of our services');
    });

    // phone number tracking for home page
    $('#home .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Homepage', 'Phone Number', 'Sticky Form');
    })

    $('#home .submit').on('click', function () {
      ga('send', 'event', 'Homepage', 'Confirmation', 'Sticky Form');
    });

    // Footer
    $('#home footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Homepage', 'click', 'Footer ' + linkName);
    });


    // Services page
    $('#services a[href="mailto:partnerreferral@firmafx.com"]').click(function () {
      ga('send', 'event', 'Our Services Page', 'email', 'partnerreferral@firmafx.com');
    });

    // phone number tracking for services page
    $('#services .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Our Services Page', 'Phone Number', 'Sticky Form');
    })

    $('#services footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Our Services Page', 'click', 'Footer ' + linkName);
    });

    //Our Company page
    $('#company .company-item a[href="management.html"]').click(function () {
      ga('send', 'event', 'Our Company Page', 'click', 'Management Team');
    });
    $('#company .company-item a[href="compliance.html"]').click(function () {
      ga('send', 'event', 'Our Company Page', 'click', 'Compliance & Accreditations');
    });
    $('#company .company-item a[href="careers.html"]').click(function () {
      ga('send', 'event', 'Our Company Page', 'click', 'Careers');
    });
    $('#company .company-item a[href="faq.html"]').click(function () {
      ga('send', 'event', 'Our Company Page', 'click', 'FAQs');
    });

    // $('#company .company-item a').on('click', function(e) {
    //   var $currentTarget = $(e.currentTarget);
    //   var linkName = $currentTarget.text();
    //   ga('send', 'event', 'Our Company Page', 'click', linkName);
    // })

    // phone number tracking company page
    $('#company .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Our Company Page', 'Phone Number', 'Sticky Form');
    })

    $('#company footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Our Company Page', 'click', 'Footer ' + linkName);
    });

    // Management
    $('#management .member-link').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var name = $currentTarget.find('.name').text();
      //console.log(name);
      ga('send', 'event', 'Management Team Sub-Page', name, 'Management Team');
    });

    // phone number tracking management page
    $('#management .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Management Team Sub-Page', 'Phone Number', 'Sticky Form');
    })

    $('#management .submit').on('click', function () {
      ga('send', 'event', 'Management Team Sub-Page', 'Confirmation', 'Sticky Form');
    })

    $('#management .secondary-nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Management Team Sub-Page', linkName, 'Sidebar');
    });

    $('#management footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Management Team Sub-Page', 'click', 'Footer ' + linkName);
    });

    // Compliance

    // phone number tracking compliance page
    $('#compliance .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Compliance and Accrediations Sub-Page', 'Phone Number', 'Sticky Form');
    })

    $('#compliance .submit').on('click', function () {
      ga('send', 'event', 'Compliance and Accrediations Sub-Page', 'Confirmation', 'Sticky Form');
    })

    $('#compliance .certificates a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var country = $currentTarget.text();
      ga('send', 'event', 'Compliance and Accrediations Sub-Page', country, 'Certificates of Insurance');
    });

    $('#compliance .secondary-nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Compliance and Accrediations Sub-Page', linkName, 'Sidebar');
    });

    $('#compliance footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Compliance and Accrediations Sub-Page', 'click', 'Footer ' + linkName);
    });


    // Careers

    // phone number tracking compliance page
    $('#careers .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Careers Sub-Page', 'Phone Number', 'Sticky Form');
    })

    $('#careers .submit').on('click', function () {
      ga('send', 'event', 'Careers Sub-Page', 'Confirmation', 'Sticky Form');
    })

    $('#careers a[href="mailto:careers@firmafx.com"]').on('click', function () {
      ga('send', 'event', 'Careers Sub-Page', 'email', 'careers@firmafx.com');
    })

    $('#careers .secondary-nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Careers Sub-Page', linkName, 'Sidebar');
    });

    $('#careers footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Careers Sub-Page', 'click', 'Footer ' + linkName);
    });

    // FAQ
    // phone number tracking faq page
    $('#faq .contact-form .phone input').focus(function () {
      ga('send', 'event', 'FAQs Page', 'Phone Number', 'Sticky Form');
    })

    $('#faq .submit').on('click', function () {
      ga('send', 'event', 'FAQs Page', 'Confirmation', 'Sticky Form');
    })

    $('#faq .secondary-nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'FAQs Page', linkName, 'Sidebar');
    });

    $('#faq footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'FAQs Page', 'click', 'Footer ' + linkName);
    });

    // Privacy Policy
    // phone number tracking faq page
    $('#privacy .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Privacy Policy Footer', 'Phone Number', 'Sticky Form');
    })
    $('#privacy .submit').on('click', function () {
      ga('send', 'event', 'Privacy Policy Footer', 'Confirmation', 'Sticky Form');
    })

    $('#privacy footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Privacy Policy Footer', 'click', 'Footer ' + linkName);
    });

    // Environmental
    $('#environment .page-content a[href="mailto:info@firmafx.com"]').on('click', function () {
      ga('send', 'event', 'Environmental Policy Footer', 'email', 'info@firmafx.com');
    });

    $('#environment .page-content a[href="https://firmafx.ca/media/13802/03202014_0011327368.pdf"]').on('click', function () {
      ga('send', 'event', 'Environmental Policy Footer', 'pdf', 'Certificate of Environmental Accomplishment');
    })

    // phone number tracking faq page
    $('#environment .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Environmental Policy Footer', 'Phone Number', 'Sticky Form');
    })
    $('#environment .submit').on('click', function () {
      ga('send', 'event', 'Environmental Footer', 'Confirmation', 'Sticky Form');
    })

    $('#environment footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Environmental Policy Footer', 'click', 'Footer ' + linkName);
    });

    // Find an Office
    $('#locations .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Find an Officer Footer', 'Phone Number', 'Sticky Form');
    })

    $('#locations .submit').on('click', function () {
      ga('send', 'event', 'Find an Office Footer', 'Confirmation', 'Sticky Form');
    })

    $('#locations footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Find an Office Footer', 'click', 'Footer ' + linkName);
    });

    // Contact us

    $('#contact .contact-form .phone input').focus(function () {
      ga('send', 'event', 'Contact Us Footer', 'Phone Number', 'Sticky Form');
    })

    $('#contact .submit').on('click', function () {
      ga('send', 'event', 'Contact Us Footer', 'Confirmation', 'Sticky Form');
    })

    $('#RootContentPlaceholder_ContentPlaceholder_ContactForm_5_cmdSubmit').on('click', function () {
      ga('send', 'event', 'Contact Us Footer', '', 'Contact Form Submit');
    })

    $('#contact footer .nav-item a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var linkName = $currentTarget.text();
      ga('send', 'event', 'Contact Us Footer', 'click', 'Footer ' + linkName);
    });


    // Contact Us USA

    $('#contact .additional-contacts .office-item-container .contact-details span[itemprop="website"] a').on('click', function (e) {
      var $currentTarget = $(e.currentTarget);
      var location = $currentTarget.parent().parent().parent().find('span[itemprop="addressLocality"]').text();
      ga('send', 'event', 'Contact Us Footer', location, 'Customer Feedback (US)');
    })

    // Manual media tracking to prevent conflicts -- captures all pages and regions
    $('.contact .submit').on('click', function () {
      if (typeof eyereturnTag !== 'null' &&
        typeof eyereturnTag !== 'undefined') {
        eyereturnTag.makeCall('submit_button');
      }
    });

  }

  return {
    init: function () {
      init();
    },
    getScrollElements: function () {
      return getScrollElements();
    }
  };
})(jQuery);
