'use strict';
/*global jQuery*/
var contactForm = (function($) {
  var $window = $(window),
      $htmlBody = $('html, body'),
      $iframeContainer = $('#iframe-container'),
      $contactPlaceholder = $('.contact-placeholder'),
      $contactContainer = $('.contact'),
      contactContainer = $contactContainer.get(0),
      $headline = $contactContainer.find('.contact-headline'),
      $contactForm = $contactContainer.find('#callback-form'),
      $formFields = $contactForm.find('.field input'),
      $requiredFields = $formFields.filter('[required]'),
      $phoneField = $formFields.filter('input[name=phone]'),
      $submitButton = $contactForm.find('.submit'),
      getUrl = $contactForm.attr('action'),
      parsleyForm;

  function initEvents() {

    // Contact form inactive/active states
    $phoneField.on('focus', expandCta);

    // Show form field placeholder when filled
    $contactContainer.find('.textinput').on('focus keyup', function(e) {
      var $currentTarget = $(e.currentTarget),
          $placeholder = $currentTarget.prev('.placeholder'),
          placeholderText = $currentTarget.attr('placeholder');

      // Show placeholder
      if ($currentTarget.val().length > 0) {
        var $label = $('<span class="placeholder">' + placeholderText + '</span>');
        if (!$placeholder.length) {
          $currentTarget.before($label);
        }
      } else {
        $placeholder.remove();
      }
    });

    // Close form
    $contactContainer.find('.hide-form').on('click', function() {
      // Clear form if complete screen is visible
      if ($contactContainer.hasClass('complete')) {
        $contactForm.trigger('reset');
      }
      resetState('fixed');
      $contactContainer.removeClass('in complete');
    });

    // Attach expand event to header
    $headline.on('click', function() {
      // Disable header click listener when form is active
      if ($contactContainer.hasClass('in') ||
          !$contactContainer.hasClass('fixed')) return;

      expandCta();
    });

    // Form actions & validation
    parsleyForm.on('field:validated', preValidate);
    $formFields.on('keyup change', preValidate);
    $contactForm.on('submit', onSubmit);
    $contactForm.on('reset', onReset);
  }

  /**
   * add masked formatting using jquery.mask.js plugin
   */
  function initInputMask() {
    // form validation for en-ca and en-fr
    $contactForm.find('.phone-format-en-ca, .phone-format-fr-ca').mask('000-000-0000');
    $contactForm.find('.postal-format-en-ca, .postal-format-fr-ca').mask('Z0Z 0Z0', {
      translation: {
        'Z': {
          // only allow letters in the Z spot in postal code format
          pattern: /[a-zA-Z]/
        }
      }
    });

    // form validation for en-us
    $contactForm.find('.phone-format-en-us').mask('000-000-0000');
    $contactForm.find('.postal-format-en-us').mask('AAAAA-BBBB', {
      translation: {
        'A': {
          pattern: /[0-9]/
        },
        'B': {
          pattern: /[0-9]/,
          optional:true
        }
      }
    });

    // form validation for en-au
    $contactForm.find('.phone-format-en-au').mask('00 0000 0000');
    $contactForm.find('.postal-format-en-au').mask('0000');

    // form validation for en-nz
    $contactForm.find('.phone-format-en-nz').mask('00-000-0000', {
      translation: {
        'Z': {
          pattern: /[0]/
        }
      }
    });
    $contactForm.find('.postal-format-en-au').mask('0000');

    // form validation for en-gb
    $contactForm.find('.phone-format-en-gb').mask('AA (AAA) AABB-BBBB', {
      translation: {
        'A': {
          pattern:/[0-9,(,), ]/
        },
        'B': {
          pattern:/[0-9,(,), ]/,
          optional:true
        }
      }
    });
    $contactForm.find('.postal-format-en-gb').mask('AAAAABBB', {
      translation: {
        'A': {
          pattern: /[0-9,a-zA-Z ]+/
        },
        'B': {
          pattern: /[0-9,a-zA-Z ]+/,
          optional:true
        }
      }
    });
  }

  /**
   * Expand the CTA area to reveal the form
   */
  function expandCta() {

    // Only execute if container isn't already visible
    if (!$contactContainer.hasClass('in')) {

      // Handle the sticky CTA differently depending on the current breakpoint
      if (utils.breakpoint(window.mobileMq) === 'mobile') {

        scrollToForm();
      } else {

        if ($contactContainer.hasClass('fixed')) {
          expandContact();
        } else {
          expandForm();
        }
      }
    }
  }

  /**
   * Animate the CTA area
   */
  function expandContact() {

    TweenLite.set(contactContainer, { y: getBreakpointFixedTransform(utils.breakpoint(window.mobileMq)) });
    TweenLite.to(contactContainer, .3, {
      y: '0%',
      ease: Power4.easeIn,
      onComplete: function() {
        expandForm();
      }
    });
  }

  /**
   * Scroll to show sticky CTA in view
   */
  function scrollToForm() {

    var position = Math.round($contactPlaceholder.offset().top - 180);

    // Jump to position
    if ($window.scrollTop() !== position) {
      $htmlBody.stop();
      $htmlBody.animate({scrollTop: position}, '1500', 'easeInOutExpo', function () {
        expandForm();
        $phoneField.focus();
        return false;
      });
    }
  }

  function preValidate() {
    if ($requiredFields.length === $formFields.filter('.parsley-success').length) {
      $submitButton.attr('disabled', false);
    } else {
      $submitButton.attr('disabled', true);
    }
  }

  function onReset() {
    $contactForm.find('.placeholder').remove();
    $submitButton.attr('disabled', true);
    parsleyForm.reset();
  }

  function onSubmit(e) {
    e.preventDefault();
    postToUrl();
    scrollToForm();
    $contactContainer.addClass('complete');
    return false; // Don't submit form
  }

  function postToUrl() {
    var formValues =  $contactForm.serializeArray(),
        postData = $.param(formValues),
        iFrameSrc = getUrl + postData,
        $postIframe = $('<iframe></iframe>').attr('src', iFrameSrc);

    $iframeContainer.append($postIframe); // Post the form by calling it through an iFrame
  }

  function init() {

    // Form validation with parsley
    parsleyForm = $contactForm.parsley();

    // initInputMask(); -- Phone and postal code fields were removed
    initEvents();
  }

  function deinit() {
    $contactContainer.find('input[name=phone]').off('focus');
    $contactContainer.find('.hide-form').on('click');
    parsleyForm.off('field:validated', preValidate);
    $formFields.off('keyup change', preValidate);
    parsleyForm.off('form:submit');
  }

  function expandForm() {
    $contactContainer.addClass('in');
  }

  function resetState(matchClass, removeClass) {
    if (!matchClass) return;

    var classToRemove = ' ' + removeClass || '';

    // Don't do anything while transitioning
    if ($contactContainer.hasClass('transitioning')) {
      return;
    }
    if ($contactContainer.hasClass(matchClass)) {
      $contactContainer.addClass('transitioning');

      // first set up the position using a set() instance
      TweenLite.set(contactContainer, { y: '0%' });
      TweenLite.to(contactContainer, .5, {
        y: getBreakpointFixedTransform(utils.breakpoint(window.mobileMq)),
        ease: Power4.easeInOut,
        onComplete: function() {
          $contactContainer.removeClass('transitioning' + classToRemove);
        }
      });
    }
  }

  function getBreakpointFixedTransform(breakpoint) {
    return breakpoint === 'desktop' ? '75%' : '85%'; // Values here should line up with CSS
  }

  function setFixed() {
    TweenLite.set(ctaContainer, { y: getBreakpointFixedTransform(utils.breakpoint(window.mobileMq)) });
    $ctaContainer.addClass('fixed').removeClass('in');
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    },
    setFixed: function() {
      setFixed()
    },
    resetCtaState: function(matchClass, removeClass) {
      resetState(matchClass, removeClass);
    }
  };
})(jQuery);
