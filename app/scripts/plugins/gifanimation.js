/**
 * gifanimation.js
 * @author: Ken Malley
 *
 * Created by Ken Malley in 2016 for dynamic banners. Repurposed for FIRMA website hero images.
 */
function GifAnimator(element, options) {

  // Only inintalize if element is provided
  if (typeof element === 'undefined') {
    console.log('No element provided, please pass in a DOM element');
    return;
  }

  this.running = true;

  // public vars too
  this.frameCount = options.frameCount;
  this.frameHold = options.frameHold;
  this.originalClassName = element.className;

  this.baseName = options.baseName;
  this.mobileFlag = options.mobileFlag;
  this.loopingFlag = options.loopingFlag;

  this._currentFrame = options.startFrame;
  this._currentFrameHold = 0;

  this._cb = function () {
  };

  //private vars
  // var _prop4 = arg4;

  this._Element = element;
  this._Element.className += ' ' + this.baseName + this.zeroPadding(this._currentFrame, 5);


  if (options.autoPlay) {
    this.play();
  }
};

GifAnimator.prototype.onComplete = function (cbFunction) {
  this._cb = cbFunction;
};

GifAnimator.prototype.play = function () {
  requestAnimationFrame(this.tick.bind(this));
};

GifAnimator.prototype.destroy = function () {
  this._Element.className = this.originalClassName;
};

GifAnimator.prototype.tick = function () {
  this.frameHoldTick(function () {
    var removeClassName = this.baseName + this.zeroPadding(this._currentFrame, 5);

    this._currentFrame++;

    if (this.loopingFlag) {
      if (this._currentFrame >= this.frameCount) {
        this._currentFrame = 0;
      }
    } else {
      if (this._currentFrame >= this.frameCount - 1) {
        this.stopAnimation();
      }
    }

    var nextClassName = this.baseName + this.zeroPadding(this._currentFrame, 5);
    var re = new RegExp('(\\s|^)' + removeClassName + '(\\s|$)', 'g');
    var newClassFrame = this._Element.className.replace(re, ' ' + nextClassName);

    this._Element.className = newClassFrame;

  }.bind(this));

  if (this.running) {
    requestAnimationFrame(this.tick.bind(this));
  }
};

GifAnimator.prototype.frameHoldTick = function (callback) {
  this._currentFrameHold++;

  if (this._currentFrameHold >= this.frameHold) {
    this._currentFrameHold = 0;
    callback();
  }
};

GifAnimator.prototype.zeroPadding = function (num, size) {
  var s = '000000000' + num;
  return s.substr(s.length - size);
};

GifAnimator.prototype.stopAnimation = function () {
  this.running = false;
  this._cb();
};

window.GifAnimator = GifAnimator;
