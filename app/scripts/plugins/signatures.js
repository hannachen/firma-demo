var sigObj = [
    // {
    //     sigWidth: 150,
    //     frameCount: 61,
    //     sigPosTop: 145,
    //     sigPosLeftPadding: 15,
    //     firmaEqualPosLeft: 70,
    //     sigStylesheet: 'css/signatures/attention.css',
    //     trackerTag: "attention"
    // },
    // {
    //     sigWidth: 165,
    //     frameCount: 61,
    //     sigPosTop: 139,
    //     sigPosLeftPadding: 3,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/boo-yeah.css',
    //     trackerTag: "boo-yeah"
    // },
    {
        sigWidth: 165,
        frameCount: 61,
        sigPosTop: 139,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/confidence.css',
        trackerTag: 'confidence'
    },
    {
        sigWidth: 155,
        frameCount: 61,
        sigPosTop: 143,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/convenient.css',
        trackerTag: 'convenient'
    },
    // {
    //     sigWidth: 170,
    //     frameCount: 61,
    //     sigPosTop: 141,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/dedication.css',
    //     trackerTag: "dedication"
    // },
    // {
    //     sigWidth: 90,
    //     frameCount: 61,
    //     sigPosTop: 139,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/done.css',
    //     trackerTag: "done"
    // },
    {
        sigWidth: 90,
        frameCount: 61,
        sigPosTop: 154,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/easy.css',
        trackerTag: 'easy'
    },
    {
        sigWidth: 180,
        frameCount: 61,
        sigPosTop: 139,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 50,
        sigStylesheet: 'css/signatures/everywhere.css',
        trackerTag: 'everywhere'
    },
    {
        sigWidth: 130,
        frameCount: 61,
        sigPosTop: 139,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/global.css',
        trackerTag: 'global'
    },
    {
        sigWidth: 130,
        frameCount: 61,
        sigPosTop: 138,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/growth.css',
        trackerTag: 'growth'
    },
    // {
    //     sigWidth: 130,
    //     frameCount: 61,
    //     sigPosTop: 138,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/guided.css',
    //     trackerTag: "guided"
    // },
    {
        sigWidth: 190,
        frameCount: 61,
        sigPosTop: 138,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 45,
        sigStylesheet: 'css/signatures/international.css',
        trackerTag: 'international'
    },
    // {
    //     sigWidth: 105,
    //     frameCount: 61,
    //     sigPosTop: 151,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/money.css',
    //     trackerTag: "money"
    // },
    // {
    //     sigWidth: 85,
    //     frameCount: 61,
    //     sigPosTop: 148,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/more.css',
    //     trackerTag: "more"
    // },
    // {
    //     sigWidth: 140,
    //     frameCount: 61,
    //     sigPosTop: 140,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/personal.css',
    //     trackerTag: "personal"
    // },
    // {
    //     sigWidth: 130,
    //     frameCount: 61,
    //     sigPosTop: 145,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/positive.css',
    //     trackerTag: "positive"
    // },
    // {
    //     sigWidth: 110,
    //     frameCount: 61,
    //     sigPosTop: 140,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/profit.css',
    //     trackerTag: "profit"
    // },
    // {
    //     sigWidth: 140,
    //     frameCount: 61,
    //     sigPosTop: 146,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/security.css',
    //     trackerTag: "security"
    // },
    // {
    //     sigWidth: 115,
    //     frameCount: 61,
    //     sigPosTop: 152,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/service.css',
    //     trackerTag: "service"
    // },
    {
        sigWidth: 115,
        frameCount: 61,
        sigPosTop: 138,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 60,
        sigStylesheet: 'css/signatures/simple.css',
        trackerTag: 'simple'
    },
    // {
    //     sigWidth: 170,
    //     frameCount: 61,
    //     sigPosTop: 140,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/specialists.css',
    //     trackerTag: "specialists"
    // },
    // {
    //     sigWidth: 130,
    //     frameCount: 61,
    //     sigPosTop: 144,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/support.css',
    //     trackerTag: "support"
    // },
    // {
    //     sigWidth: 80,
    //     frameCount: 61,
    //     sigPosTop: 144,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/time.css',
    //     trackerTag: "time"
    // },
    // {
    //     sigWidth: 90,
    //     frameCount: 61,
    //     sigPosTop: 144,
    //     sigPosLeftPadding: 5,
    //     firmaEqualPosLeft: 60,
    //     sigStylesheet: 'css/signatures/trust.css',
    //     trackerTag: "trust"
    // },
    {
        sigWidth: 240,
        frameCount: 61,
        sigPosTop: 139,
        sigPosLeftPadding: 5,
        firmaEqualPosLeft: 20,
        sigStylesheet: 'css/signatures/uncomplicated.css',
        trackerTag: 'uncomplicated'
    }
];
