// Handle breakpoints using JS
breakpoints.init();

// Initialize nav events
nav.init();

// Initialize hero slideshow
hero.init();

// Initialize tracking
eventTagging.init();

// Initialize team list accordion
team.init();

// Initialize locations list accordion
locations.init();

// Initialize careers list accordion
careers.init();

// Initialize contact page dropdown
contact.init();

// Initialize footer contact form
contactForm.init();

// Rest of the stuff can go here, but please create a module whenever possible!

// Scroll to top on footer logo click
var $htmlBody = $('html, body');
$('footer .firma-icon').on('click', function(e) {
  e.preventDefault();
  $htmlBody.stop();
  $htmlBody.animate({scrollTop: 0}, '1500', 'easeInOutExpo', function () {
    return false;
  });
});

// set a cookie to check whether to set the cookie popup for NZ/AU

$('.close-cookie-popup').on('click', function(e) {
  e.preventDefault();
  Cookies.set('cookie-policy', '1', {expires: 365 });
  // close cookie popup
  $('.cookie-popup').hide();
});

if(Cookies.get('cookie-policy')) {
  console.log('true');
  $('.cookie-popup').hide();
} else {
  $('.cookie-popup').show();
}

// Set up CTA animation
var $currencySymbol = $('.contact-icon .currency-symbol'),
    animationTime = 4.5,
    total = $currencySymbol.length,
    repeatDelay = animationTime * (total - 1);

// Animate currency symbols using GreenSock
$currencySymbol.each(function(i, v) {
  var startDelay = (animationTime * i) + (i > 0 ? 0 : .25),
      timeline = new TimelineMax({repeat: -1, delay: startDelay, repeatDelay: repeatDelay, paused: false});

  timeline.set(v, { yPercent: 155 });
  timeline.to(v, .75, { yPercent: 0, ease: Back.easeOut.config(1.7) });
  timeline.to(v, 2, { yPercent: 0 });
  timeline.to(v, 1, { yPercent: -155, ease: Back.easeIn.config(1.7) });
});

// Handle scroll events
var $window = $(window),
    $ctaPlaceholder = $('.contact-placeholder'),
    ctaPlaceholder = utils.setAsArray($ctaPlaceholder),
    $ctaContainer = $('.contact'),
    ctaContainer = $ctaContainer.get(0),
    trackingElements = eventTagging.getScrollElements(),
    isScrolling;

$window.on('scroll', function() {
  if(!isScrolling) {
    window.requestAnimationFrame(function onRequestAnimationFrame() {
      elementsInView(ctaPlaceholder, window.ctaOffset);
      elementsInView(trackingElements);
      if (breakpoints.isDesktop()) {
        contactForm.resetCtaState('fixed in', 'in');
      }
      isScrolling = false;
    });
    isScrolling = true;
  }
});

// Set initial state
if (!utils.isElementsInView(ctaContainer, window.ctaOffset)) {
  contactForm.setFixed();
}
utils.isElementsInView(trackingElements);

$ctaContainer.on('inview', function() {
});

$ctaPlaceholder.on('inview', function() {

  TweenLite.set(ctaContainer, { y: '0%' });
  $ctaContainer.removeClass('fixed transitioning in');
});

$ctaPlaceholder.on('outview', function() {
  contactForm.setFixed();
});

/**
 * Check if elements are in view
 */
function elementsInView(elements, offsetTop) {

  var offset = offsetTop || 0;

  // Set up plugin to detect whether an element is visible in the viewport
  for (let i = 0; i < elements.length; i++) {
    let $el = elements[i],
        inView = utils.isElementsInView($el.get(0), offset);

    // Emit event
    if (inView && !$el.hasClass('in')) {
      $el.trigger({
        type: 'inview'
      });
    } else if (!inView && $el.hasClass('in')) {
      $el.trigger({
        type: 'outview'
      });
    }

    // Update element state
    setInView($el, inView);
  }
}

/**
 * Add or remove 'in' class based on the element's in view status.
 * @param {jQuery} $el - Element to check for and apply class to
 * @param {boolean} inView - Element's in view status
 */
function setInView($el, inView) {
  if (inView) {
    $el.addClass('in');
  } else {
    $el.removeClass('in');
  }
}