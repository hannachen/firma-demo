'use strict';
/*global jQuery*/
var team = (function($) {
  var $teamListLinks = $('.team-list .member-link');

  function onTeamClick(e) {
    e.preventDefault();
    var $currentTarget = $(e.currentTarget),
        $parent = $currentTarget.closest('.team-item'),
        $target = $parent.find('.description');

    if ($parent.hasClass('in')) { // Currently open
      $target.collapse('hide');
      $parent.removeClass('in');

    } else { // Not open
      $target.collapse('show');
      $parent.addClass('in');
    }
  }

  function init() {

    $teamListLinks.on('click', onTeamClick);
  }

  function deinit() {

    $teamListLinks.off('click', onTeamClick);
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
