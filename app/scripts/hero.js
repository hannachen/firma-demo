'use strict';
/*global jQuery*/
var hero = (function($) {
  var $hero = $('.hero, .hero-secondary'),
      $currentSlide = $hero.filter('.current') || $hero.first(),
      duration = 9000,
      transition = 500,
      totalSlides = $hero.length,
      totalTime = duration + transition,
      timeouts = [],
      zIndexDefault = 60,
      initialized = false;

  function animate(i, slide) {
    var $slide = $(slide);

    // Start of animation cycle
    var timeout = setTimeout(function() {
      $slide.addClass('current').css('z-index', 70);
      $currentSlide = $slide;

      let $handwritingSprites = $slide.find('.handwriting .sprite');

      // Make sure handwriting sprite is there
      if ($handwritingSprites.length) {

        // Make ths sprite container visible
        $handwritingSprites.addClass('in');

        // Set hero text animation
        let handwritingSprite = $handwritingSprites.get(0),
            options = {
            },
            handWritingAnimation = new GifAnimator(handwritingSprite, {
              frameCount: 120,
              frameHold: 1,
              baseName: 'frame_',
              startFrame: 0,
              autoPlay: false,
              loopingFlag: false
            });

        handWritingAnimation.onComplete(function() {
          handWritingAnimation.destroy();
        });

        handWritingAnimation.play();
      }

      // Fade out animation
      setTimeout(function() {
        $slide.addClass('fadeOut').delay(transition).queue(function(next) {
          $(this).css('z-index', 0).removeClass('fadeOut');
          next();
        });
      }, duration);

      // Clear active state
      setTimeout(function() {
        $slide.removeClass('current');
      }, totalTime);

      // Handle last slide
      if (i === totalSlides - 1) {
        $hero.first().css('z-index', zIndexDefault);
      }

    }, totalTime * i);
    timeouts.push(timeout);

    // Restart animation
    var animationTimeout = setTimeout(function() {
          animate(i, slide);
          $slide.find('.handwriting .sprite').removeClass('in');
          $slide.css('z-index', zIndexDefault - i);
        }, totalTime * totalSlides);
    timeouts.push(animationTimeout);
  }

  function zIndexOrder(i, slide) {
    $(slide).css('z-index', zIndexDefault - i);
  }

  function init() {

    // Multiple (carousel)
    if ($hero.length > 1) {
      $hero.each(zIndexOrder);
      $hero.each(animate);
      initialized = true;

    // Single (banner)
    } else if ($hero.length === 1) {

      // TODO: refactor the section below so it's not repeating in this file
      let $handwritingSprites = $hero.find('.handwriting .sprite');

      // Make sure handwriting sprite is there
      if ($handwritingSprites.length) {

        // Make ths sprite container visible
        $handwritingSprites.addClass('in');

        // Set hero text animation
        let handwritingSprite = $handwritingSprites.get(0),
          handWritingAnimation = new GifAnimator(handwritingSprite, {
            frameCount: 120,
            frameHold: 1,
            baseName: 'frame_',
            startFrame: 0,
            autoPlay: false,
            loopingFlag: false
          });

        handWritingAnimation.onComplete(function () {
          handWritingAnimation.destroy();
        });

        handWritingAnimation.play();
      }

      initialized = true;
    }
  }

  function deinit() {
    _.forEach(function(timeouts) {
      clearTimeout(timeouts);
    });
    initialized = false;
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
