'use strict';
/*global jQuery, enquire*/
var breakpoints = (function($) {
  window.mobileMq = 'screen and (min-width: 48em)';
  var $toggleElements = $('[data-toggle=position]'),
      mobileCtaOffset = 70, // This is used when mobile and desktop offset are different
      desktopCtaOffset = 70;

  function toggleElement(type) {
    var matchType = type || 'desktop';
    $toggleElements.each(function() {
      var $element = $(this),
          insertPosition = $element.data('position'),
          toggleType = $element.data('toggle-on'),
          $parentContainer = $element.parent(),
          $targetContainer = toggleType === matchType ? $($element.data('container')) : $($element.data('parent'));

      // Only insert item if the target is different from the current parent
      if (!$parentContainer.is($targetContainer)) {
        insertItem($element, $targetContainer, insertPosition);
      }
    });
  }

  function insertItem($element, $target, position) {
    if (position === 'before') {
      $element.detach().prependTo($target);
    } else {
      $element.detach().appendTo($target);
    }
  }

  function init() {
    // Handle breakpoints using JS
    // Move secondary nav into the sidebar on content pages to prevent duplicated bav dom elements (for SEO)
    // Move quotes into quotes container
    enquire.register(window.mobileMq, {
      setup: function() {
        toggleElement(utils.breakpoint(window.mobileMq));
        if (utils.breakpoint(window.mobileMq) === 'mobile') {
          mobileQuotes.init();
          // not sure why but this broke mobile so i commented it out
          //video.deinit();
          window.ctaOffset = mobileCtaOffset;
        } else {
          window.ctaOffset = desktopCtaOffset;
        }
      },
      match: function() {
        toggleElement('desktop');
        mobileQuotes.deinit();
        stickyHero.init();
        video.init();
        window.ctaOffset = desktopCtaOffset;
      },
      unmatch: function() {
        toggleElement('mobile');
        mobileQuotes.init();
        stickyHero.deinit();
        video.deinit();
        window.ctaOffset = mobileCtaOffset;
      }
    });

    // Prevent quotes click-through
    $('.quote-link').on('click', function(e) {
      e.preventDefault();
    });
  }

  function deinit() {

    enquire.unregister(window.mobileMq);
  }

  function isDesktop() {
    return window.matchMedia(window.mobileMq).matches;
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    },
    isDesktop: function() {
      return isDesktop();
    }
  };
})(jQuery);
