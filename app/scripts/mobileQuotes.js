'use strict';
/*global jQuery*/
var mobileQuotes = (function($) {
  var $quoteThumbnails = $('.quote-link'),
      $quoteContainer = $('.mobile-quotes-container'),
      $activeQuote = $quoteThumbnails.filter('.active');

  function onQuoteClick(e) {
    $quoteThumbnails.removeClass('active');
    var $currentTarget = $(e.currentTarget),
        $quoteContents = $currentTarget.find('.quote-overlay').clone().addClass('in'),
        $fadeOut = $quoteContainer.find('.quote-overlay').removeClass('in');
    $currentTarget.addClass('active');
    $quoteContents.appendTo($quoteContainer).addClass('in');
    setTimeout(function() {
      $fadeOut.remove();
    }, 500);
  }

  function init() {
    $quoteThumbnails.on('click', onQuoteClick);
    if (_.isEmpty($activeQuote)) {
      $activeQuote = $quoteThumbnails.first();
      $activeQuote.trigger('click');
    }
  }

  function deinit() {
    $quoteThumbnails.off('click', onQuoteClick).removeClass('active');
    $quoteContainer.empty();
    $activeQuote = null;
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
