'use strict';
/*global jQuery*/
var stickyHero = (function($) {
  var $window = $(window),
      $body = $('body'),
      $contentContainer = $('.main-content'),
      $secondaryHero = $body.find('.hero-secondary'),
      debounceOptions = {
        leading: true,
        trailing: false
      };

  function onResize() {
    var margin = ($body.width() - $contentContainer.width()) / 2;
    $secondaryHero.css('margin-right','-' + margin + 'px');
  }

  function init() {
    onResize();
    if ($secondaryHero.length) {
      $window.on('resize', _.debounce(onResize, debounceOptions));
    }
  }

  function deinit() {
    if ($secondaryHero.length) {
      $window.off('resize', onResize);
    }
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
