'use strict';
/*global jQuery*/
var contact = (function($) {
  var $officeListLinks = $('.office-contact-list .office-contact-link'),
      $officeContacts = $('.office-contact .location'),
      $contactContainer = $('.contact-form-container'),
      $iframeContainer = $('#iframe-container'),
      $contactForm = $contactContainer.find('#contact-page-form'),
      $formFields = $contactForm.find('.field input'),
      $requiredFields = $formFields.filter('[required]'),
      $submitButton = $contactForm.find('.submit'),
      getUrl = $contactForm.attr('action'),
      parsleyForm;

  function onClick(e) {
    e.preventDefault();

    var $currentTarget = $(e.currentTarget),
        $target = $officeContacts.filter($currentTarget.attr('href'));

    if (!$target.hasClass('in')) { // Only show if entry is not already visible
      $officeContacts.removeClass('in');
      $target.addClass('in');
    }
  }

  function onSubmit(e) {
    e.preventDefault();
    postToUrl();
    $contactContainer.addClass('complete');
    return false; // Don't submit form
  }

  function postToUrl() {
    var formValues =  $contactForm.serializeArray(),
        postData = $.param(formValues),
        iFrameSrc = getUrl + postData,
        $postIframe = $('<iframe></iframe>').attr('src', iFrameSrc);

    $iframeContainer.append($postIframe); // Post the form by calling it through an iFrame
  }

  function preValidate() {
    if ($requiredFields.length === $formFields.filter('.parsley-success').length) {
      $submitButton.attr('disabled', false);
    } else {
      $submitButton.attr('disabled', true);
    }
  }

  function initEvents() {

    // Form actions & validation
    parsleyForm.on('field:validated', preValidate);
    $formFields.on('keyup change', preValidate);
    $contactForm.on('submit', onSubmit);
    $officeListLinks.on('click', onClick);
  }

  function init() {

    // Initialize only when contact form is present
    if ($contactForm.length) {

      // Form validation with parsley
      parsleyForm = $contactForm.parsley();

      initEvents();
    }
  }

  function deinit() {

    parsleyForm.off('field:validated', preValidate);
    $formFields.off('keyup change', preValidate);
    $contactForm.off('submit', onSubmit);
    $officeListLinks.off('click', onClick);
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);