function init() {
    var sigAnim = new GifAnimator({
        frameCount: 60,
        frameHold: 1,
        IDSelector: 'signature',
        baseName: 'frame_',
        startFrame: 0,
        autoPlay: false,
        loopingFlag: false
    });

    sigAnim.onComplete(function() {
        //do something when completed
    });

    sigAnim.play();
}


/** HTML element
    <div id="signature" class="sprite-sig"></div>

    <style>
    #signature {
        display: block;
        position: absolute;
        top: 128px;
        left: 11px;
    }
    </style>
**/
