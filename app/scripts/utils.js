'use strict';
/*global jQuery*/
(function () {
  // requestAnimationFrame Polyfill
  var lastTime = 0;
  var vendors = ['ms', 'moz', 'webkit', 'o'];
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback) {
      var currTime = new Date().getTime();
      var timeToCall = Math.max(0, 16 - (currTime - lastTime));
      var id = window.setTimeout(function () {
          callback(currTime + timeToCall);
        },
        timeToCall);
      lastTime = currTime + timeToCall;
      return id;
    };
  }
  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id);
    };
  }
}());

var utils = (function ($, window) {
  return {
    /**
     * Search for url query parameter name.
     *
     * @param String url
     * @param String name
     */
    getUrlParameterByName: function(url, name) {
      if (!url) {
        url = window.location.href;
      }
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)', 'i'),
        results = regex.exec(url);
      if (!results) {
        return null;
      }
      if (!results[2]) {
        return '';
      }
      return decodeURIComponent(results[2].replace(/\+/g, ''));
    },
    getParameters: function() {
      var url = window.location.href,
        urlParts = url.split('?'),
        results = urlParts.length,
        params = [];

      if (results) {
        let urlParams = urlParts[1];
        if (urlParams.length) {
          let paramSet = urlParams.split('&');
          for (let i = 0; i < paramSet.length; i++) {
            let paramSetArray = paramSet[i].split('=');
            if (paramSetArray.length) {
              params[paramSetArray[0]] = paramSetArray[1]; // Set key value pair
            }
          }
        }
      }

      return params;
    },
    getQueryStrings: (function() {
      var url = window.location.href,
        urlParts = url.split('?'),
        queryString = '';

      if (urlParts.length > 1) {
        queryString = urlParts[urlParts.length - 1];
      }

      return queryString;
    })(),
    getUrlHash: function() {
      var url = window.location.href,
        urlParams = url.split('?'),
        results = urlParams.length,
        hash = '';

      if (results) {
        for (let i = 0; i < results; i++) {
          if (urlParams[i].length) {
            let urlHash = urlParams[i].split('#');
            if (urlHash.length > 1) {
              hash = '#' + urlHash[urlHash.length - 1]; // Return last item in array
            }
          }
        }
      }
      return hash;
    },

    /**
     * Checks if elements are in view.
     * @param element - element to check
     */
    isElementsInView: function(element, offsetTop) {

      // return false if there is no element
      if (!element) return false;

      // variables; position of the element, window size, viewport size
      var offset = offsetTop || 0,
          elementPos = getAbsoluteOffset(element) + offset,
          elementHeight = element.offsetHeight,
          windowHeight = window.innerHeight || document.documentElement.clientHeight,
          viewportY = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop,
          viewportBottomY = viewportY + windowHeight,
          elementPosBottom = elementPos + elementHeight + offset;

      // return if the element is in view
      if ( (viewportBottomY >= elementPos && viewportY <= elementPos)
        || (viewportBottomY >= elementPosBottom && viewportY <= elementPosBottom)
        || (viewportY >= elementPos && viewportBottomY <= elementPosBottom)) {
        return true;
      } else {
        return false;
      }

      // cross-browser way of getting an elements absolute offset
      function getAbsoluteOffset(element) {
        var offset = 0;
        while (element != null) {
          offset += element.offsetTop;
          element = element.offsetParent;
        }
        return offset;
      }
    },

    /**
     * Put jQuery selected elements into an array
     * @param {jQuery} $el - jQuery selection
     * @returns {Array} newArray - array of jQuery selected elements
     */
    setAsArray: function($el) {
      let newArray = [];
      $.each($el, function(i, v) {
        newArray.push($(v));
      });
      return newArray;
    },

    /**
     * Checks if the user is using IE browsers user agent string.
     */
    isIe: (function() {
      // Detects IE
      if(navigator.appName === 'Microsoft Internet Explorer' ||
        navigator.userAgent.indexOf('Trident/') > -1 ||
        /(Edge\/\d+)/.test(navigator.userAgent)) {
        return true;
      }
      return false;
    })(),
    breakpoint: function(mediaMq) {
      return window.matchMedia(mediaMq).matches ? 'desktop' : 'mobile';
    }

  };
})(jQuery, window);
