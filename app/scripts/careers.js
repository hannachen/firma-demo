'use strict';
/*global jQuery*/
var careers = (function($) { // TODO: Refator and abstract out this file, locations, team.js
  var $regionListLinks = $('.region-list .region-link');

  function onClick(e) {
    e.preventDefault();
    var $currentTarget = $(e.currentTarget),
        $parent = $currentTarget.closest('.region-group'),
        $target = $parent.find('.job-list');

    if ($parent.hasClass('in')) { // Currently open
      $target.collapse('hide');
      $parent.removeClass('in');

    } else { // Not open
      $target.collapse('show');
      $parent.addClass('in');
    }
  }

  function init() {

    $regionListLinks.on('click', onClick);
  }

  function deinit() {

    $regionListLinks.off('click', onClick);
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
