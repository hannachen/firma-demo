'use strict';
/*global jQuery*/
var locations = (function($) { // TODO: Refator and abstract out this file and team.js
  var $locationListLinks = $('.locations-list .region-link');

  function onLocationClick(e) {
    e.preventDefault();
    var $currentTarget = $(e.currentTarget),
        $parent = $currentTarget.closest('.location-item'),
        $target = $parent.find('.offices');

    if ($parent.hasClass('in')) { // Currently open
      $target.collapse('hide');
      $parent.removeClass('in');

    } else { // Not open
      $target.collapse('show');
      $parent.addClass('in');
    }
  }

  function init() {

    $locationListLinks.on('click', onLocationClick);
  }

  function deinit() {

    $locationListLinks.off('click', onLocationClick);
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
