'use strict';
/*global jQuery*/
var nav = (function($) {
  var $navBrand = $('.navbar-brand'),
      $mainNav = $('#main-nav');

  function init() {
    $mainNav.on('show.bs.collapse hide.bs.collapse', toggleClass);
  }

  function toggleClass(e) {
    switch(e.type) {
      case 'show':
      case 'shown':
        $navBrand.addClass('collapsed');
        break;
      case 'hide':
      case 'hidden':
        $navBrand.removeClass('collapsed');
        break;
    }
  }

  function deinit() {
    $mainNav.off('show.bs.collapse hide.bs.collapse', toggleClass);
  }

  return {
    init: function() {
      init();
    },
    deinit: function() {
      deinit();
    }
  };
})(jQuery);
