'use strict';
/*global jQuery*/
var video = (function($) {

  /**
   * FUNCTION TO GET AND AUTO PLAY YOUTUBE VIDEO FROM DATATAG
   */
  function autoPlayYoutubeModal() {
    var $trigger = $('body').find('[data-toggle="modal"]');

    $trigger.click(function (e) {
      var $theModal = $(e.currentTarget.getAttribute('data-target')),
        $modalIframe = $theModal.find('iframe'),
        videoSRC = $(this).attr('data-thevideo'),
        videoSRCauto = videoSRC + '?autoplay=1&showinfo=0&rel=0&autohide=1&iv_load_policy=3';

      $modalIframe.attr('src', videoSRCauto);
      $theModal.find('.close').click(function () {
        $modalIframe.attr('src', videoSRC);
      });

      $('.modal').click(function () {
        $modalIframe.attr('src', videoSRC);
      });
    });
  }

  function init() {
    autoPlayYoutubeModal();
  }

  return {
    init: function() {
      init();
    }
  };
})(jQuery);
