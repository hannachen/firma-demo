# FIRMA Foreign Exchange 2016 Re-design

## About this project

### Stack & Structure
- This project is mobile first, when defining CSS rules, default applies to the smallest breakpoint.
- Generated with yeoman generator-webapp v2.1.0 with SASS, Bootstrap (v4.0.0-alpha.2 pre-release), and Modernizr
- Template engine: Handlebars -- generated static pages
- Production is a .NET server running Umbraco WCMS platform Version 4.9.1

### Setup
To get the site running locally, run:

- `npm install`
- `bower install`
- `gulp serve`

### CMS Contents
- This site is based off a CMS, meaning we must work with consistent data to be re-used. To avoid copy pasting,
repeatable data is saved in `data/*.json`. The `.json` file should be named after the page's html file name. For example:
data for **careers.html** should be stored in **data/careers.json**

### Production site CMS & Hosting
- Runs on ASP.Net CMS platform [Umbraco](https://umbraco.com/), version 4.9.1.
- Lives on Microsoft Azure's cloud hosting.
- Hosting is managed by client's IT team
- Content is managed by marketing and HR (careers section)

---

## Hosting

#### Staging
http://firmafx.cloudapp.net/   
Hosted on client's MS Azure cloud server. Can only be accessed from whitelisted IPs.

#### Production
Hosted on client's MS Azure cloud server. CMS access is granted on a need basis. Main contact is 
Mike Rue <Mike.Rue@firmafx.com> IT Infrastructure Manager.

---

## Links

#### Staging
- [ternstone.ca](http://ternstone.ca) (Canada)
- [ternstone.com](http://ternstone.com/en-us) (US)
- [ternstone.com.au](http://ternstone.com.au) (Australia)
- [ternstone.co.uk](http://ternstone.co.uk) (UK)
- [ternstone.co.nz](http://ternstone.co.nz/) (New Zealand)

#### Production
- [firmafx.ca](https://firmafx.ca) (Canada)
- [firmafx.com](https://firmafx.com) (US)
    - [careers.firmafx.com](https://careers.firmafx.com) (Jobs)
- [firmafx.com.au](https://firmafx.com.au) (Australia)
- [firmafx.co.uk](https://firmafx.co.uk) (UK)
- [firmafx.co.nz](https://firmafx.co.nz) (New Zealand)

---

## Deployment

### Deploy to a dev server
`gulp deploy` is available to upload static files to a dev server to preview the site. Please make sure to run `gulp build` before deploying.

Note: `gulp build` may take a while (~10min) to run.

### Deploy to production server
Due to time constraints, the production site isn't set up with proper CMS content and templates. Each page
is tied to a static theme rather than content slots. There is a manual step to translate content blocks into the CMS' templates.

1. Run `gulp templates` to build all Handlebars partials.
2. Find and extract HTML snippets from `.tmp` directory.
3. Move the code to the files in the proper country folder in `/umbraco-files`
4. Each template goes into **Settings > Templates > Firma-2016**
5. Each template files links to a CMS content page within **Content**