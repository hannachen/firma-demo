<section class="contact-placeholder"></section>
<div class="contact-anchor">
  <section class="contact">

    <div class="container contact-container">
      <button class="hide-form">
        <span class="close"><span class="icon icon-firma-close"></span></span>
      </button>

      <h1 class="contact-headline">
        <button class="headline-cta">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                	 width="200px" height="200px" viewBox="0 0 200 200" enable-background="new 0 0 200 200" preserveAspectRatio="xMinYMin meet"
                   shape-rendering="geometricPrecision" xml:space="preserve" class="contact-icon">

                  <defs>
                    <clipPath id="bubbleMask" clipPathUnits="userSpaceOnUse">
                      <path fill="#ffffff" d="M100.1,166.5c-13.1,0-25.8-2.4-37.9-7.1c-0.9-0.4-1.9-0.5-2.8-0.5c-1.3,0-2.6,0.3-3.8,1l-31,17.7l11-27.5
                	c1.2-2.9,0.4-6.3-1.9-8.5C18.4,127.5,10,109.3,10,90.4c0-42,40.4-76.1,90.1-76.1c49.7,0,90.1,34.2,90.1,76.1
                	C190.2,132.3,149.8,166.5,100.1,166.5z"/>
                    </clipPath>
                  </defs>

            <!-- Currency symbol goes here -->
                  <g class="currencyGroup" clip-path="url(#bubbleMask)">
                    <path class="icon-path currency-symbol euro" d="M66.7,86.9h-8.5V74.8h10.9C75.3,57.3,91.6,47,109.5,47c7.3,0,11.3,1,15.6,2.7l-4.2,14.5
                	c-2.7-1-6.3-1.8-11.3-1.8c-8.6,0-17.1,3.7-21.7,12.4h30.1l-3.5,12.1h-30c-0.3,1.7-0.3,3-0.3,4.2c0,1.8,0.1,3.1,0.3,4h28.1l-3.5,12.1
                	H88.2c4.6,7.7,12.7,11.5,21.6,11.5c4.9,0,8.6-0.8,11.3-1.8l4,14.4c-3.6,1.7-8.9,3.1-15.3,3.1c-18.1,0-34.2-9.8-40.5-27.1H58.2V95.1
                	h8.6c-0.3-1.9-0.3-4-0.3-4.6C66.6,89.7,66.6,88.7,66.7,86.9" />
                    <path class="icon-path currency-symbol dollar" d="M95.7,146.3v-12.9c-4.2-0.6-7.8-1.7-11-3.4c-3.1-1.7-5.7-3.7-7.8-6c-2.1-2.3-3.7-4.8-4.9-7.5
                	c-1.2-2.7-1.9-5.3-2.1-7.9l14.9-3.6c0.2,1.9,0.6,3.8,1.4,5.5c0.8,1.7,1.8,3.3,3.2,4.6c1.4,1.4,3,2.4,5.1,3.2c2,0.8,4.4,1.2,7.2,1.2
                	c3.9,0,7-0.8,9.1-2.5c2.2-1.7,3.3-3.9,3.3-6.5c0-2.1-0.7-3.9-2.2-5.4c-1.5-1.5-3.8-2.5-6.8-3.2L94,99.7c-6.5-1.4-11.6-4.1-15.4-8.1
                	c-3.8-3.9-5.7-8.8-5.7-14.6c0-3.2,0.6-6.2,1.7-8.9c1.2-2.7,2.8-5.1,4.8-7.2c2-2.1,4.5-3.8,7.2-5.2c2.8-1.4,5.8-2.3,9-2.8V40h12.3
                	v13.2c3.4,0.6,6.4,1.7,9,3.1c2.5,1.4,4.7,3.1,6.4,4.9c1.7,1.8,3.1,3.8,4.1,6c1,2.1,1.7,4.2,2.1,6.2l-14.8,4.2c-0.2-1-0.5-2.2-1-3.5
                	c-0.5-1.3-1.3-2.5-2.4-3.7c-1.1-1.2-2.4-2.1-4.1-2.9c-1.6-0.8-3.7-1.1-6.2-1.1c-3.8,0-6.8,0.9-8.8,2.8c-2,1.9-3.1,4.1-3.1,6.6
                	c0,1.8,0.7,3.5,2,4.9c1.4,1.4,3.4,2.5,6.3,3.1l10.9,2.5c7.6,1.7,13.2,4.6,16.7,8.8c3.5,4.2,5.3,9,5.3,14.4c0,2.8-0.5,5.5-1.4,8.1
                	c-1,2.6-2.4,4.9-4.3,7c-1.9,2.1-4.3,3.9-7,5.4c-2.8,1.5-6,2.5-9.6,3.1v13H95.7z"/>
                    <path class="icon-path currency-symbol pound" d="M78.9,82.1c-1.2-3.3-2.1-6.8-2.1-11.2c0-11.9,9.6-22.8,25-22.8c18.4,0,24.6,12.5,24.7,22.2l-14.1,2.1
                	c-0.1-7-3.9-11.2-10.3-11.2c-5.3,0-10.1,3.2-10.1,9.9c0,4,1.2,7.3,2.6,10.9H116v12H98.2c0,0.8,0.1,1.6,0.1,2.5
                	c0,6.5-2.7,12.5-8.5,16h18.2c5.9,0,9.5-4.2,9.5-9.9l14,1.2c0,12.9-8.3,22.3-21,22.3H71.2v-13c7.5-2.7,12.5-8.1,12.5-14.9
                	c0-1.4-0.2-2.8-0.5-4.1H71.7v-12H78.9z"/>
                    <path class="icon-path currency-symbol yen" d="M109.4,118.7v16.2H93.7v-16.2H72.9v-11.3h20.8V99H72.9V87.8h12.9L67.1,56.8h18.2l16.6,29.5l16.4-29.5h17.5
                	l-18.5,30.9H131V99h-21.7v8.4H131v11.3H109.4z"/>
                  </g>

                  <path class="icon-path speech-bubble" d="M100.1,5.1C45.7,5.1,1.5,43.5,1.5,90.7c0,20,8.1,39.4,22.9,54.8L8.5,185.2c-1.2,3.1-0.4,6.6,2.1,8.7
                	c1.4,1.2,3.2,1.9,5.1,1.9c1.4,0,2.7-0.4,3.9-1L63,170c11.9,4.2,24.4,6.3,37.1,6.3c54.4,0,98.6-38.4,98.6-85.6
                	C198.7,43.5,154.5,5.1,100.1,5.1z M100.1,164.5c-12.7,0-25.1-2.3-36.8-6.9c-0.9-0.3-1.8-0.5-2.7-0.5c-1.3,0-2.6,0.3-3.7,1
                	l-30.2,17.2l10.7-26.7c1.1-2.8,0.4-6.2-1.9-8.2c-14.8-13.7-23-31.4-23-49.8c0-40.8,39.3-74,87.6-74c48.3,0,87.6,33.2,87.6,74
                	C187.7,131.3,148.4,164.5,100.1,164.5z" />

                </svg>
          Learn how Firma can <span class="soft-break mobile"></span>help
          <span class="nobr">your business.</span>
        </button>
      </h1>
      <form action="//go.pardot.com/l/108482/2016-09-14/cc9jt?" method="post" class="contact-form" id="callback-form" data-parsley-trigger="keyup blur">
        <div class="field first">
          <input name="first" type="text" class="textinput" autocomplete="off" placeholder="First name" maxlength="20"
                 data-parsley-length="[2, 20]" data-parsley-length-message="Name should be between 2 and 20 characters" required=""/>
        </div>
        <div class="field email">
          <input name="email" type="email" class="textinput" autocomplete="off" placeholder="Your email address" maxlength="40"
                 data-parsley-length="[8, 40]" data-parsley-type-message="Invalid email address" required=""/>
        </div>
        <div class="button">
          <button type="submit" class="submit" disabled>Submit</button>
        </div>
        <p class="legal">
          By providing your email address you agree to receive digital communications from Firma Foreign Exchange. You
          may unsubscribe <span class="nobr">at anytime.</span>
        </p>
      </form>
      <div class="complete-message">
        <h1>Nice to meet you.</h1>
        <p>
          We&rsquo;ll be in touch shortly.
          <span class="nobr">Thank you.</span>
        </p>
      </div>

    </div>

    <div id="iframe-container"></div>

  </section>
</div>