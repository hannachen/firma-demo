<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactFormOnly.ascx.cs" Inherits="FirmaFx.Web.UserControls.ContactForm" %>

<asp:UpdatePanel ID="FormContainer" runat="server" UpdateMode="Conditional">
<ContentTemplate>
    <div class="hidden">

        <div class="contactAddress firmaAddress">
            <div class="addressTitle"><asp:Literal ID="ltrlAddressTitle" runat="server" /></div>
            <ui:FirmaLocation ID="uiLocation" runat="server" />
        </div>

        <div class="dropdown" id="LocationDropDownContainer" runat="server">
            <button class="btn btn-default dropdown-toggle btn-block" type="button" id="ddm1" data-toggle="dropdown" aria-expanded="true">
                <umbraco:Item field="#ContactLocationButton" runat="server" /> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu scrollable-menu" role="menu" aria-labelledby="ddm1">
                <asp:Repeater ID="rGroups" runat="server">
                    <ItemTemplate>
                        <li class="dropdown-header"><%# Container.DataItem.ToString() %></li>
                        <asp:Repeater ID="rLocations" runat="server" ItemType="FirmaFx.Web.Models.FirmaLocation">
                            <ItemTemplate>
                                <li role="presentation">
                                    <asp:LinkButton role="menuitem" ID="lbLocation" runat="server" OnClick="lbLocation_Click" CommandArgument="<%# Item.ID %>" CausesValidation="false"><%# Item.Title %></asp:LinkButton>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
    </div>

    <div class="form-group reason">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-fw fa-question-circle"></i></span>
            <asp:DropDownList ID="ddlReason" runat="server" CssClass="form-control" AutoPostBack="true" />
        </div>
        <asp:RequiredFieldValidator ID="rfvReason" runat="server" Display="Dynamic" CssClass="error" SetFocusOnError="true" ControlToValidate="ddlReason" InitialValue="0" />
        <asp:Label ID="lblLocationMessage" runat="server" Visible="false" CssClass="message"><umbraco:Item field="#ContactLocationMessage" runat="server" /></asp:Label>
    </div>

    <div ID="pnlLocations" class="form-group" style="display:none;">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-fw fa-globe"></i></span>
            <asp:DropDownList ID="ddlLocations" runat="server" CssClass="form-control" />
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-fw fa-user"></i></span>
            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" required="required" />
        </div>
        <asp:RequiredFieldValidator ID="rfvName" runat="server" Display="Dynamic" CssClass="error" SetFocusOnError="true" ControlToValidate="txtName" />
    </div>

    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-fw fa-envelope-o"></i></span>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" required="required" />
        </div>
        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" Display="Dynamic" CssClass="error" SetFocusOnError="true" ControlToValidate="txtEmail" />
        <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" CssClass="error" SetFocusOnError="true" ControlToValidate="txtEmail" />
    </div>

    <div class="form-group">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-fw fa-phone"></i></span>
            <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"  />
        </div>
    </div>

    <div class="form-group">
        <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control" TextMode="MultiLine" rows="8" required="required" />
    </div>

    <asp:LinkButton ID="cmdSubmit" runat="server" CssClass="btn btn-default btn-block"><%: umbraco.library.GetDictionaryItem("Submit") %></asp:LinkButton>

</ContentTemplate>
<Triggers>
    <asp:PostBackTrigger ControlID="cmdSubmit" />
</Triggers>
</asp:UpdatePanel>