<%@ Master Language="C#" MasterPageFile="~/masterpages/Firma-2016.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootStylePlaceholder" runat="server">
</asp:content>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
    <umbraco:Macro Alias="StaticCookiePolicy-en" runat="server" />
    <asp:ContentPlaceHolder Id="RootContentPlaceholder" runat="server" />
    <umbraco:Macro Alias="FooterContactForm-en-gb" runat="server" />
    <umbraco:Macro Alias="StaticFooter-en-gb" runat="server" />
</asp:content>

<asp:content ContentPlaceHolderId="RootAnalyticsPlaceholder" runat="server">
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
              function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-19092405-7','auto'); ga('send','pageview');
    </script>
</asp:content>