<%@ Master Language="C#" MasterPageFile="~/masterpages/fr-ca.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior nosidebar">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/fr-ca/" class="brand-link">

            <?xml version="1.0" encoding="utf-8"?>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                       xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="258px" height="42.1px" viewBox="0 0 258 42.1"
                       preserveAspectRatio="xMinYMin meet" shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo"
                       enable-background="new 0 0 258 42.1">
                    <path d="M43.86,40.58H36.09L36.08,1h7.77V40.58Zm31.69-16.4a13.83,13.83,0,0,0,7.24-12c0-11-12-11.13-12-11.13H51.09V40.54h7.7V25.7H68l8.08,14.8,8.94,0Zm-0.31-11.1c0.05,5.17-6,5-6,5H58.93V8.64H70C75.78,8.75,75.24,13.08,75.24,13.08ZM28.85,8.7V1.08H8.68C4.58,1.08,1.36,3.64,1,7,1,7.12,1,40.53,1,40.53H8.74l0-15.63,18.26,0V17.16l-18.15,0V8.7h20Zm100.65-3a4.83,4.83,0,0,0-4.78-4.61,4.77,4.77,0,0,0-4.13,2.43h0l-10.7,18.95S99.33,3.83,99.25,3.72a4.77,4.77,0,0,0-9.08,2s0,0.08,0,.12,0,0.08,0,.12V40.6h7.71V15.92l9,16.84h6l8.83-16.84V40.6h7.78S129.5,5.77,129.5,5.68Zm28.36-2.21a4.62,4.62,0,0,0-8.17,0l-15,37.07h8.63L147,31h13.54l3.85,9.55h8.56Zm-8.32,21.19,4.27-12.93,4.35,12.93h-8.62Zm29.64-2.88a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2,3.1,3.1,0,0,0-3.08,3.39,3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,21.78Zm16.34,1.45a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,195.52,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,194,23.23Zm6.77-1.69a3.37,3.37,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06H197V20h1.49v1a1.94,1.94,0,0,1,1.86-1.11,2.18,2.18,0,0,1,.41,0v1.56ZM202,28.95V20h1.48v0.87a2.3,2.3,0,0,1,2-1c1.9,0,3,1.45,3,3.35a3.07,3.07,0,0,1-3,3.37,2.32,2.32,0,0,1-1.95-.88v3.23H202Zm3.23-7.71a1.78,1.78,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.77,1.77,0,0,0,205.26,21.24Zm10.88,2a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,216.14,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,214.6,23.23Zm6.77-1.69a3.35,3.35,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06h-1.53V20h1.49v1A1.94,1.94,0,0,1,221,19.94a2.16,2.16,0,0,1,.41,0v1.56Zm2.83,1.23,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4v3.19a5.87,5.87,0,0,0,.08,1h-1.42a3.54,3.54,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1,2,2,0,0,1-2.17-1.94A2,2,0,0,1,224.2,22.77Zm2.12,1V23.49l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,226.33,23.78ZM231.58,20h1.33v1.36h-1.33v3a0.7,0.7,0,0,0,.82.8,3.35,3.35,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V21.38h-1.19V20h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm3.65-3.26a1,1,0,0,1,1,1A1,1,0,1,1,235.23,16.77Zm-0.75,9.67V20H236v6.42h-1.52Zm9.65-3.22a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,244.13,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,242.59,23.23Zm4.55,3.22H245.6V20h1.49v0.86a2.13,2.13,0,0,1,1.9-1,2.27,2.27,0,0,1,2.32,2.54v4.06h-1.53v-3.8a1.24,1.24,0,0,0-1.32-1.42,1.35,1.35,0,0,0-1.33,1.52v3.7Zm-68,9.52a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2A3.1,3.1,0,0,0,180.8,36a3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,36Zm11.74,1,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4V39.6a5.8,5.8,0,0,0,.08,1H193.2a3.57,3.57,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1A2,2,0,0,1,189,38.89,2,2,0,0,1,190.92,37ZM193,38V37.68l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,193,38Zm3.35,2.66V34.21h1.46V35a2.19,2.19,0,0,1,1.9-1,2,2,0,0,1,1.92,1.13A2.25,2.25,0,0,1,203.75,34,2.2,2.2,0,0,1,206,36.47v4.16h-1.48V36.72a1.17,1.17,0,0,0-1.24-1.32A1.33,1.33,0,0,0,202,36.82v3.81h-1.5V36.72a1.17,1.17,0,0,0-1.24-1.32,1.31,1.31,0,0,0-1.33,1.42v3.81h-1.52Zm11.5,0V31.09h1.5V35a2.31,2.31,0,0,1,2-1c1.92,0,3,1.46,3,3.35s-1.15,3.4-3,3.4a2.2,2.2,0,0,1-2-1v0.86h-1.49Zm3.22-5.23a1.77,1.77,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.75,1.75,0,0,0,211.11,35.4ZM216.6,31a1,1,0,0,1,1,1A1,1,0,1,1,216.6,31Zm-0.75,9.67V34.21h1.52v6.42h-1.52Zm4.26-2.11a1.2,1.2,0,0,0,1.3,1.09,0.82,0.82,0,0,0,1-.74,0.77,0.77,0,0,0-.75-0.71L220.67,38a1.94,1.94,0,0,1-1.73-1.87A2.23,2.23,0,0,1,221.31,34a2.26,2.26,0,0,1,2.48,1.79l-1.29.37a1.1,1.1,0,0,0-1.19-1,0.83,0.83,0,0,0-.92.74,0.7,0.7,0,0,0,.65.67l1,0.2a2,2,0,0,1,1.85,2,2.18,2.18,0,0,1-2.43,2.06,2.41,2.41,0,0,1-2.65-1.94Zm7.2-4.31h1.33v1.36H227.3v3a0.7,0.7,0,0,0,.82.8,3.34,3.34,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V35.57H224.6V34.21h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm8.51,4.59a2.89,2.89,0,0,1-2.9,2,3.41,3.41,0,0,1-.16-6.81,3,3,0,0,1,3.14,3.33,2.25,2.25,0,0,1,0,.49h-4.69a1.72,1.72,0,0,0,1.75,1.67,1.55,1.55,0,0,0,1.61-1.12Zm-1.46-2.08a1.44,1.44,0,0,0-1.58-1.42,1.54,1.54,0,0,0-1.57,1.42h3.15Z" transform="translate(-1 -1)"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/fr-ca/services/">
                Nos services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link" href="/fr-ca/company/">
                Notre entreprise
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/fr-ca/team/" class="secondary-nav-link">Équipe de gestion</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/compliance/" class="secondary-nav-link">Conformité <span class="soft-break"></span>&
                    Accréditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Carrières</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/fr-ca/blog/">
                Notre blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Canada <span>FR</span>
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australie</a>

                <a class="dropdown-item" href="https://firmafx.co.nz">Nouvelle-Zélande </a>

                <a class="dropdown-item" href="https://firmafx.co.uk">Royaume-Uni</a>

                <a class="dropdown-item" href="/en-us/">États-Unis</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="main-content container">

      <div class="keyline"></div>
      <div class="page-content">

        <section class="container light">

          <h1>Politique sur la confidentialité</h1>

          <p>FIRMA Corporation Cambiste s'engage à protéger les renseignements personnels et confidentiels qui lui sont
            confiés par ses clients. Les renseignements personnels et confidentiels s'entendent de l'information concernant
            une personne ou une société identifiable, ce qui peut inclure le nom, l'adresse, le numéro de téléphone, un
            numéro d'identification, de l'information financière, etc., sur une personne ou une société.</p>


          <h2>Collecte de renseignements personnels et confidentiels</h2>

          <p>FIRMA Corporation Cambiste ne recueille que les renseignements qui lui sont nécessaires pour procurer des
            services de qualité supérieure aux clients. Cela inclut les renseignements nécessaires pour :</p>

          <ul>
            <li>Établir une relation personnelle avec chaque client</li>
            <li>Offrir efficacement (soit en personne ou par voie électronique) les produits et services demandés</li>
            <li>Fournir l'information spécifique sur le marché approprié selon les besoins de chaque client</li>
            <li>Faire un suivi auprès des clients pour déterminer leur degré de satisfaction quant aux produits et
              services
            </li>
            <li>Notifier les clients des activités d'intérêt à venir</li>
            <li>Accorder des crédits</li>
            <li>Répondre aux exigences réglementaires.</li>
          </ul>

          <p>FIRMA Corporation Cambiste recueille normalement des renseignements sur les clients directement auprès de ses
            clients. FIRMA Corporation Cambiste peut recueillir des renseignements sur les clients d'autres personnes avec
            le consentement du client ou tel qu'autorisé par la Loi.</p>

          <p>FIRMA Corporation Cambiste informe ses clients des fins pour lesquelles elle recueille les renseignements
            personnels, avant ou au moment de recueillir ces renseignements. L'exception à cette règle est lorsque le client
            propose de l'information volontairement pour une fin évidente (dans ce cas, le consentement du client est
            supposé) ou dans des circonstances particulières où la collecte, l'utilisation ou la divulgation sans
            consentement est autorisée ou exigée par la Loi.</p>

          <p>FIRMA Corporation Cambiste peut demander le consentement de ses clients à certaines fins, mais peut ne pas être
            en mesure de fournir certains services si le client n'est pas disposé à consentir à la collecte, à l'utilisation
            ou à la divulgation de certains renseignements. Lorsque le consentement explicite du client est nécessaire,
            FIRMA Corporation Cambiste demandera normalement aux clients de fournir un consentement verbal (à savoir, en
            personne, par téléphone), par écrit (à savoir, en signant un formulaire de consentement, en cochant une case sur
            un formulaire) ou par voie électronique (à savoir, en cliquant sur un bouton).</p>

          <p>Un client peut retirer son consentement à l'utilisation et à la divulgation des renseignements personnels en
            tout temps, à moins que ces renseignements personnels ne soient nécessaires pour que FIRMA Corporation Cambiste
            puisse remplir ses obligations légales. FIRMA Corporation Cambiste respectera votre décision, mais peut ne pas
            être en mesure de vous offrir certains produits et services si elle ne possède pas les renseignements personnels
            nécessaires.</p>

          <p>FIRMA Corporation Cambiste peut recueillir, utiliser ou divulguer les renseignements personnels et
            confidentiels des clients sans consentement seulement si la Loi l'autorise. Par exemple, FIRMA Corporation
            Cambiste peut ne pas demander de consentement lorsque la collecte, l'utilisation ou la divulgation est
            raisonnable aux fins d'une enquête ou d'une procédure judiciaire; par exemple, dans le cas du recouvrement d'une
            créance en souffrance, dans une situation d'urgence qui met en danger la vie, la santé ou la sécurité, ou
            lorsque les renseignements personnels sont extraits d'un annuaire téléphonique public.</p>


          <h2>Utilisation des renseignements confidentiels</h2>

          <p>FIRMA Corporation Cambiste utilise et divulgue des renseignements sur les clients seulement aux fins auxquelles
            les renseignements ont été recueillis, sauf dans les cas autorisés par la Loi. Par exemple, FIRMA Corporation
            Cambiste peut utiliser les coordonnées du client pour s'assurer qu’il reçoit le meilleur service possible. La
            Loi permet aussi à FIRMA Corporation Cambiste d'utiliser ces coordonnées aux fins du recouvrement d'une créance
            en souffrance, le cas échéant.</p>

          <p>FIRMA Corporation Cambiste ne ménage aucun effort raisonnable pour s'assurer que les renseignements sur le
            client sont exacts et complets. FIRMA Corporation Cambiste compte sur le client pour qu'il l'avise de tout
            changement lié à ses renseignements qui peut influer sur sa relation avec FIRMA Corporation Cambiste. Dans
            certains cas, FIRMA Corporation Cambiste peut exiger une demande de correction par écrit.</p>


          <h2>Sécurité</h2>

          <p>FIRMA Corporation Cambiste protège les renseignements sur les clients d'une manière appropriée en raison de la
            confidentialité de l'information et tel que requis par les lois en vigueur. Les renseignements sur les clients
            sont enregistrés en format électronique, dans un système informatique hautement sécurisé qui ne peut être
            consulté que par des personnes à l'emploi de FIRMA Corporation Cambiste. Les documents papier sont stockés dans
            une installation d’entreposage protégée et conservés aussi longtemps que la Loi l'exige. FIRMA Corporation
            Cambiste ne ménage aucun effort raisonnable pour prévenir toute perte, mauvaise utilisation, divulgation ou
            modification des renseignements personnels, ainsi que tout accès non autorisé à de tels renseignements. Le
            maintien de la sécurité des renseignements personnels et confidentiels est d'une importance capitale pour FIRMA
            Corporation Cambiste.</p>

          <p>FIRMA Corporation Cambiste utilise des mesures de sécurité appropriées pour détruire les renseignements sur les
            clients, incluant le déchiquetage des dossiers papier et la suppression définitive des documents électroniques.
            FIRMA Corporation Cambiste conserve les renseignements personnels des clients aussi longtemps qu'il est
            raisonnable pour atteindre les fins auxquelles les renseignements ont été recueillis, ou à des fins juridiques
            ou commerciales.</p>


          <h2>Droits d'accès</h2>

          <p>Les clients de FIRMA Corporation Cambiste ont un droit d'accès à leurs propres renseignements personnels
            stockés dans un dossier sous la garde ou le contrôle de FIRMA Corporation Cambiste, sous réserve de certaines
            exceptions. Par exemple, FIRMA Corporation Cambiste peut refuser de fournir l’accès à de l'information qui
            pourrait révéler des renseignements personnels concernant une autre personne ou société. L'accès peut également
            être refusé si l'information est privilégiée ou se trouve dans des dossiers de médiation.</p>

          <p>Si FIRMA Corporation Cambiste refuse d'accéder à une demande en tout ou en partie, elle en fournira les
            raisons. Dans certains cas, lorsque des exceptions à l'accès s'appliquent, FIRMA Corporation Cambiste peut
            retenir l'information en cause et fournir au client le reste du dossier.</p>

          <p>Bien que FIRMA Corporation Cambiste s'efforce de répondre à chaque demande aussitôt que possible, la
            récupération des renseignements peut prendre jusqu'à 45 jours. FIRMA Corporation Cambiste peut imputer des frais
            raisonnables pour fournir les renseignements, mais non pour apporter une correction. FIRMA Corporation Cambiste
            vous informera de tout frais pouvant s'appliquer avant de commencer à traiter votre demande.</p>

          <p>Si vous avez des questions ou des préoccupations au sujet de toute collecte, utilisation ou divulgation de
            renseignements personnels par FIRMA Corporation Cambiste, ou d'une demande d'accès à vos renseignements
            personnels, veuillez communiquer avec votre FIRMA Corporation Cambiste local.</p>

        </section>

      </div>

    </div>
	</div>
</asp:content>