<%@ Master Language="C#" MasterPageFile="~/masterpages/fr-ca.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
  <div class="interior">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/fr-ca/" class="brand-link">

            <?xml version="1.0" encoding="utf-8"?>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                       xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="258px" height="42.1px" viewBox="0 0 258 42.1"
                       preserveAspectRatio="xMinYMin meet" shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo"
                       enable-background="new 0 0 258 42.1">
                    <path d="M43.86,40.58H36.09L36.08,1h7.77V40.58Zm31.69-16.4a13.83,13.83,0,0,0,7.24-12c0-11-12-11.13-12-11.13H51.09V40.54h7.7V25.7H68l8.08,14.8,8.94,0Zm-0.31-11.1c0.05,5.17-6,5-6,5H58.93V8.64H70C75.78,8.75,75.24,13.08,75.24,13.08ZM28.85,8.7V1.08H8.68C4.58,1.08,1.36,3.64,1,7,1,7.12,1,40.53,1,40.53H8.74l0-15.63,18.26,0V17.16l-18.15,0V8.7h20Zm100.65-3a4.83,4.83,0,0,0-4.78-4.61,4.77,4.77,0,0,0-4.13,2.43h0l-10.7,18.95S99.33,3.83,99.25,3.72a4.77,4.77,0,0,0-9.08,2s0,0.08,0,.12,0,0.08,0,.12V40.6h7.71V15.92l9,16.84h6l8.83-16.84V40.6h7.78S129.5,5.77,129.5,5.68Zm28.36-2.21a4.62,4.62,0,0,0-8.17,0l-15,37.07h8.63L147,31h13.54l3.85,9.55h8.56Zm-8.32,21.19,4.27-12.93,4.35,12.93h-8.62Zm29.64-2.88a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2,3.1,3.1,0,0,0-3.08,3.39,3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,21.78Zm16.34,1.45a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,195.52,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,194,23.23Zm6.77-1.69a3.37,3.37,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06H197V20h1.49v1a1.94,1.94,0,0,1,1.86-1.11,2.18,2.18,0,0,1,.41,0v1.56ZM202,28.95V20h1.48v0.87a2.3,2.3,0,0,1,2-1c1.9,0,3,1.45,3,3.35a3.07,3.07,0,0,1-3,3.37,2.32,2.32,0,0,1-1.95-.88v3.23H202Zm3.23-7.71a1.78,1.78,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.77,1.77,0,0,0,205.26,21.24Zm10.88,2a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,216.14,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,214.6,23.23Zm6.77-1.69a3.35,3.35,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06h-1.53V20h1.49v1A1.94,1.94,0,0,1,221,19.94a2.16,2.16,0,0,1,.41,0v1.56Zm2.83,1.23,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4v3.19a5.87,5.87,0,0,0,.08,1h-1.42a3.54,3.54,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1,2,2,0,0,1-2.17-1.94A2,2,0,0,1,224.2,22.77Zm2.12,1V23.49l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,226.33,23.78ZM231.58,20h1.33v1.36h-1.33v3a0.7,0.7,0,0,0,.82.8,3.35,3.35,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V21.38h-1.19V20h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm3.65-3.26a1,1,0,0,1,1,1A1,1,0,1,1,235.23,16.77Zm-0.75,9.67V20H236v6.42h-1.52Zm9.65-3.22a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,244.13,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,242.59,23.23Zm4.55,3.22H245.6V20h1.49v0.86a2.13,2.13,0,0,1,1.9-1,2.27,2.27,0,0,1,2.32,2.54v4.06h-1.53v-3.8a1.24,1.24,0,0,0-1.32-1.42,1.35,1.35,0,0,0-1.33,1.52v3.7Zm-68,9.52a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2A3.1,3.1,0,0,0,180.8,36a3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,36Zm11.74,1,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4V39.6a5.8,5.8,0,0,0,.08,1H193.2a3.57,3.57,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1A2,2,0,0,1,189,38.89,2,2,0,0,1,190.92,37ZM193,38V37.68l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,193,38Zm3.35,2.66V34.21h1.46V35a2.19,2.19,0,0,1,1.9-1,2,2,0,0,1,1.92,1.13A2.25,2.25,0,0,1,203.75,34,2.2,2.2,0,0,1,206,36.47v4.16h-1.48V36.72a1.17,1.17,0,0,0-1.24-1.32A1.33,1.33,0,0,0,202,36.82v3.81h-1.5V36.72a1.17,1.17,0,0,0-1.24-1.32,1.31,1.31,0,0,0-1.33,1.42v3.81h-1.52Zm11.5,0V31.09h1.5V35a2.31,2.31,0,0,1,2-1c1.92,0,3,1.46,3,3.35s-1.15,3.4-3,3.4a2.2,2.2,0,0,1-2-1v0.86h-1.49Zm3.22-5.23a1.77,1.77,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.75,1.75,0,0,0,211.11,35.4ZM216.6,31a1,1,0,0,1,1,1A1,1,0,1,1,216.6,31Zm-0.75,9.67V34.21h1.52v6.42h-1.52Zm4.26-2.11a1.2,1.2,0,0,0,1.3,1.09,0.82,0.82,0,0,0,1-.74,0.77,0.77,0,0,0-.75-0.71L220.67,38a1.94,1.94,0,0,1-1.73-1.87A2.23,2.23,0,0,1,221.31,34a2.26,2.26,0,0,1,2.48,1.79l-1.29.37a1.1,1.1,0,0,0-1.19-1,0.83,0.83,0,0,0-.92.74,0.7,0.7,0,0,0,.65.67l1,0.2a2,2,0,0,1,1.85,2,2.18,2.18,0,0,1-2.43,2.06,2.41,2.41,0,0,1-2.65-1.94Zm7.2-4.31h1.33v1.36H227.3v3a0.7,0.7,0,0,0,.82.8,3.34,3.34,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V35.57H224.6V34.21h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm8.51,4.59a2.89,2.89,0,0,1-2.9,2,3.41,3.41,0,0,1-.16-6.81,3,3,0,0,1,3.14,3.33,2.25,2.25,0,0,1,0,.49h-4.69a1.72,1.72,0,0,0,1.75,1.67,1.55,1.55,0,0,0,1.61-1.12Zm-1.46-2.08a1.44,1.44,0,0,0-1.58-1.42,1.54,1.54,0,0,0-1.57,1.42h3.15Z" transform="translate(-1 -1)"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/fr-ca/services/">
                Nos services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link active" href="/fr-ca/company/">
                Notre entreprise
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/fr-ca/team/" class="secondary-nav-link active">Équipe de gestion</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/compliance/" class="secondary-nav-link">Conformité <span class="soft-break"></span>&
                    Accréditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Carrières</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/fr-ca/blog/">
                Notre blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Canada <span>FR</span>
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australie</a>

                <a class="dropdown-item" href="https://firmafx.co.nz">Nouvelle-Zélande </a>

                <a class="dropdown-item" href="https://firmafx.co.uk">Royaume-Uni</a>

                <a class="dropdown-item" href="/en-us/">États-Unis</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="main-content container">

      <div class="row-sidebar">
        <div class="sidebar"></div>
        <div class="page-content">

          <section class="hero hero-secondary" style="background-image:url(/images/company/background-management.jpg)" ;>
            <div class="contents container">
              <div class="text">
                <div class="firma-equals">
                  <img src="/images/text-firma-equels-fr.svg" class="text-firma-equals">
                  <div class="handwriting">
                    <div class="sprite engagement"></div>

                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="container light">
            <h1>Rencontrez notre équipe <span class="soft-break mobile"></span>de gestion.</h1>
            <ul class="team-list accordion">

              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Dave Dominy</span>
                    <span class="job-title">Directeur Général</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Dave Dominy s’est joint à Firma en juin 2014 comme directeur général, apportant à l’entreprise une
                    expertise et une expérience considérables dans le domaine des finances. Il a passé une quinzaine
                    d’années à occuper diverses fonctions de cadre supérieur dans le secteur des services bancaires et
                    d’investissement pour une banque à charte canadienne, et huit ans comme directeur général d’une société
                    d’investissement dans l’ouest du Canada. En tant que directeur général d’un organisme de réglementation
                    provincial, il a directement participé à l’adoption de nouvelles normes en matière de liquidités et de
                    capital pour les coopératives d’épargne et de crédit. De 1999 à 2003, il a été directeur général d’une
                    société ouverte de taille moyenne œuvrant dans le domaine des services pétroliers et, avant cela, a été
                    directeur financier d’une société ouverte à forte capitalisation de traitement et de transport de gaz.
                  </p>
                  <p>
                    Durant sa longue carrière d’administrateur, M. Dominy a siégé au conseil d’administration de divers
                    organismes à but non lucratif, d’entreprises privées et ouvertes et de sociétés d’État. Il occupe
                    actuellement les fonctions de vice-président et administrateur du Collège Norquest, de président du
                    comité d’audit et administrateur de Chandos Construction LP, d’administrateur de l’Alberta Insurance
                    Council, de président de 3D Capital Inc. et d’instructeur au programme de gouvernance de l’Alberta
                    School of Business pour les petites et moyennes entreprises.
                  </p>
                  <p>
                    Il contribue activement à de nombreux organismes de bienfaisance locaux, tout récemment comme président
                    de la Stollery Children’s Hospital Foundation à Edmonton.
                  </p>
                  <p>
                    M. Dominy est titulaire d’une maîtrise en gestion de la J.L. Kellogg School of Business de Chicago et
                    d’un baccalauréat en mathématiques de l’Université de Winnipeg, en plus d’avoir suivi le Cours sur le
                    commerce des valeurs mobilières au Canada et de détenir le titre IAS.A de l’Institut des administrateurs
                    de sociétés du Canada.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Michael Oshry</span>
                    <span class="job-title">Président</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Michael a entamé sa carrière en démarrant une variété d'entreprises locales dans les secteurs de la
                    vente au détail, de l'hôtellerie et des services aux entreprises. Cofondateur de FIRMA Corporation
                    Cambiste en 1998, il a été l'un des deux lauréats du Grand Prix de l’Entrepreneur Ernst & Young en 2007.
                  </p>
                  <p>
                    Michael a été élu pour représenter le quartier 5 au sein du conseil municipal d'Edmonton en octobre
                    2013. Il a ainsi siégé à une variété de comités et de conseils et a contribué à diverses initiatives,
                    notamment à titre de membre du City Manager and City Auditor Performance Evaluation Committee, de
                    président du Transportation Committee, de membre du Licence and Community Standards Appeal Board et de
                    représentant au sein d'Edmonton Northlands, de l’Emerging Economy Initiative et de
                    l'<a href="http://www.edmontonpolicecommission.com/" target="_blank">Edmonton Police Commission</a>.
                  </p>
                  <p>
                    Auparavant, il a également été membre du conseil d'administration de la section albertaine de la Young
                    Presidents' Organization (YPO) et a siégé à divers conseils d'administration d'autres organismes à but
                    non lucratif.
                  </p>
                  <p>
                    Michael est marié et le fier père de deux enfants.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">James Devenish</span>
                    <span class="job-title">Directeur général des ventes</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    James est arrivé chez Firma Corporation Cambiste avec plus de 17 ans d’expérience dans le secteur de la
                    finance. Avant de se joindre à Firma en 2011, il a occupé des postes avec la Western Union Business
                    Solutions (anciennement Custom House) et HSBC Financial Canada, où il a perfectionné ses qualités
                    d’opérateur et de vendeur en traitant autant avec des clients internationaux que nationaux.
                  </p>
                  <p>
                    Depuis qu’il s’est joint à Firma, James a constamment démontré sa passion à livrer des résultats en
                    matière de bénéfices en se concentrant sur le développement d’équipes très performantes, la gestion des
                    ventes et la planification stratégique. De nombreuses ventes, la croissance de l’entreprise et plusieurs
                    réalisations rentables tout au long de sa carrière sont des exemples de son succès.
                  </p>
                  <p>
                    En tant que directeur général des ventes, James concentre ses efforts à développer de nouvelles
                    occasions d’affaires et s’assure que le commerce international continue de fournir une expérience de
                    ventes et de services de qualité supérieure à tous les clients. Son expérience et ses connaissances de
                    l’industrie ainsi que ses forces en leadership, développement de talents et prestation de programmes
                    font de lui le choix idéal pour diriger nos équipes de vente à l’international.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Alex Eadie, CPA, CMA, CAMS</span>
                    <span class="job-title">Directeur général des finances</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Alex Eadie, comptable en management accrédité (CMA) et spécialiste certifié en anti-blanchiment d’argent
                    (CAMS), a plus 12 ans d’expérience en tant que cadre dans le domaine. Récemment, en tant que comptable
                    en chef et directeur général, Alex a dirigé la croissance rapide d’une entreprise chef de file en
                    matière d’intégration logicielle.
                  </p>
                  <p>
                    Pour améliorer le côté compétitif de l’entreprise, Alex apporte son esprit professionnel et créatif afin
                    de promouvoir de saines pratiques commerciales avec une spécialité marquée pour les techniques de
                    gestion. Le flair financier d’Alex, sa vision stratégique et son approche collaborative pour implanter
                    une variété d’initiatives sont essentiels au maintien de la croissance internationale actuelle de Firma
                    Corporation Cambiste.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Brad Clark, BComm, BSc</span>
                    <span class="job-title">Directeur général des opérations</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Brad Clark est arrivé chez FIRMA Corporation Cambiste avec plus de huit ans d’expérience en gestion des
                    opérations. Il détient un baccalauréat en commerce de l’Université d’Alberta et un baccalauréat en
                    science des mathématiques. Les aptitudes de Brad en communication et en leadership, de pair avec sa
                    capacité à bien travailler sous pression, ont contribué en grande partie à son succès.
                  </p>
                  <p>
                    Avant d’être avec FIRMA Corporation Cambiste, Brad était le directeur général du plus important
                    fournisseur d’impression commerciale de l’Ouest canadien, une division de l’une des plus importantes
                    corporations internationales en médias imprimés. Durant ces années, Brad a été identifié comme l’un des
                    75 employés au plus grand potentiel en Amérique du Nord et a reçu une formation et développement de
                    carrière dans le cadre du Programme leadership, excellence et développement.
                  </p>
                  <p>
                    En tant que directeur général des opérations, Brad coordonne et gère une équipe diversifiée de
                    professionnels, incluant les services de technologie de l’information, les communications et le
                    personnel de négociation. Le dévouement et la persévérance de Brad aident à la croissance stratégique de
                    l’entreprise et soutiennent les composantes opérationnelles essentielles au succès continu de FIRMA
                    Corporation Cambiste.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Chris Brockbank</span>
                    <span class="job-title">Directeur général du marketing</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Chris apporte près de 30 ans d’expérience en marketing à l’équipe de FIRMA Corporation Cambiste. Les
                    expériences précédentes de Chris comprennent des postes de directeur général du marketing chez Sears
                    Canada, Moneris, Just Energy et Sun/Québecor Media. De plus, Chris a également occupé des postes
                    importants en marketing chez Sirius Canada, Bell/Bell Wireless et Nortel Networks.
                  </p>
                  <p>
                    En plus de ses vastes connaissances et de son expérience, Chris est également professeur à l’Université
                    de Toronto où il enseigne la gestion de marque dans un programme de formation continue.
                  </p>
                  <p>
                    En tant que directeur général du marketing, Chris va rechercher, implanter et découvrir des moyens de
                    pousser la marque, les produits et les services de Firma à un autre niveau. Il sait comment mettre de
                    l’avant notre avantage concurrentiel dans le marché afin de propulser Firma vers une croissance accrue à
                    l’international.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Gordon Wollenberg, BCom, CHRP</span>
                    <span class="job-title">Directeur général talent</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Gordon Wollenberg s’est joint à Firma en mars 2013 avec plus de 20 ans d’expérience en ressources
                    humaines. Il détient un baccalauréat en commerce de l’Université d’Alberta et a obtenu son titre de
                    conseiller en ressources humaines agréé auprès du HRIA.
                  </p>
                  <p>
                    Avant de se joindre à Firma, Gordon était directeur des ressources humaines pour une importante
                    entreprise de construction canadienne. Il a précédemment travaillé avec la ville d’Edmonton, le
                    gouvernement de l’Alberta, ATB et le Catholic School Board.
                  </p>
                  <p>
                    En tant que directeur général talent, Gordon est responsable de l’ensemble des opérations entourant les
                    ressources humaines chez Firma. Gordon et son équipe se concentrent sur le recrutement d’individus de
                    haut calibre, ainsi que sur des initiatives d’apprentissage et de perfectionnement qui assurent la
                    performance et le succès continu de Firma.
                  </p>
                </div>
              </li>
            </ul>
          </section>

        </div>
      </div>

    </div>
  </div>
</asp:content>