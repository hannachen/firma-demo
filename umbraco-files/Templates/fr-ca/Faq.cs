<%@ Master Language="C#" MasterPageFile="~/masterpages/fr-ca.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/fr-ca/" class="brand-link">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="258px" height="42.1px" viewBox="0 0 258 42.1" preserveAspectRatio="xMinYMin meet" shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo" enable-background="new 0 0 258 42.1"><path d="M43.86,40.58H36.09L36.08,1h7.77V40.58Zm31.69-16.4a13.83,13.83,0,0,0,7.24-12c0-11-12-11.13-12-11.13H51.09V40.54h7.7V25.7H68l8.08,14.8,8.94,0Zm-0.31-11.1c0.05,5.17-6,5-6,5H58.93V8.64H70C75.78,8.75,75.24,13.08,75.24,13.08ZM28.85,8.7V1.08H8.68C4.58,1.08,1.36,3.64,1,7,1,7.12,1,40.53,1,40.53H8.74l0-15.63,18.26,0V17.16l-18.15,0V8.7h20Zm100.65-3a4.83,4.83,0,0,0-4.78-4.61,4.77,4.77,0,0,0-4.13,2.43h0l-10.7,18.95S99.33,3.83,99.25,3.72a4.77,4.77,0,0,0-9.08,2s0,0.08,0,.12,0,0.08,0,.12V40.6h7.71V15.92l9,16.84h6l8.83-16.84V40.6h7.78S129.5,5.77,129.5,5.68Zm28.36-2.21a4.62,4.62,0,0,0-8.17,0l-15,37.07h8.63L147,31h13.54l3.85,9.55h8.56Zm-8.32,21.19,4.27-12.93,4.35,12.93h-8.62Zm29.64-2.88a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2,3.1,3.1,0,0,0-3.08,3.39,3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,21.78Zm16.34,1.45a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,195.52,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,194,23.23Zm6.77-1.69a3.37,3.37,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06H197V20h1.49v1a1.94,1.94,0,0,1,1.86-1.11,2.18,2.18,0,0,1,.41,0v1.56ZM202,28.95V20h1.48v0.87a2.3,2.3,0,0,1,2-1c1.9,0,3,1.45,3,3.35a3.07,3.07,0,0,1-3,3.37,2.32,2.32,0,0,1-1.95-.88v3.23H202Zm3.23-7.71a1.78,1.78,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.77,1.77,0,0,0,205.26,21.24Zm10.88,2a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,216.14,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,214.6,23.23Zm6.77-1.69a3.35,3.35,0,0,0-.5,0,1.61,1.61,0,0,0-1.73,1.88v3.06h-1.53V20h1.49v1A1.94,1.94,0,0,1,221,19.94a2.16,2.16,0,0,1,.41,0v1.56Zm2.83,1.23,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4v3.19a5.87,5.87,0,0,0,.08,1h-1.42a3.54,3.54,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1,2,2,0,0,1-2.17-1.94A2,2,0,0,1,224.2,22.77Zm2.12,1V23.49l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,226.33,23.78ZM231.58,20h1.33v1.36h-1.33v3a0.7,0.7,0,0,0,.82.8,3.35,3.35,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V21.38h-1.19V20h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm3.65-3.26a1,1,0,0,1,1,1A1,1,0,1,1,235.23,16.77Zm-0.75,9.67V20H236v6.42h-1.52Zm9.65-3.22a3.33,3.33,0,1,1-6.66,0A3.33,3.33,0,1,1,244.13,23.23Zm-1.54,0a1.8,1.8,0,1,0-3.58,0A1.81,1.81,0,1,0,242.59,23.23Zm4.55,3.22H245.6V20h1.49v0.86a2.13,2.13,0,0,1,1.9-1,2.27,2.27,0,0,1,2.32,2.54v4.06h-1.53v-3.8a1.24,1.24,0,0,0-1.32-1.42,1.35,1.35,0,0,0-1.33,1.52v3.7Zm-68,9.52a4.68,4.68,0,0,1,4.7-4.88,4.07,4.07,0,0,1,4.2,3l-1.49.5a2.59,2.59,0,0,0-2.72-2A3.1,3.1,0,0,0,180.8,36a3.09,3.09,0,0,0,3.1,3.35,2.79,2.79,0,0,0,2.78-2.06l1.45,0.47a4.19,4.19,0,0,1-4.23,3.1A4.63,4.63,0,0,1,179.18,36Zm11.74,1,1.65-.25a0.45,0.45,0,0,0,.47-0.46,1,1,0,0,0-1.21-1,1.24,1.24,0,0,0-1.32,1.16l-1.4-.32a2.51,2.51,0,0,1,2.7-2.1c2,0,2.73,1.12,2.73,2.4V39.6a5.8,5.8,0,0,0,.08,1H193.2a3.57,3.57,0,0,1-.07-0.82,2.24,2.24,0,0,1-2,1A2,2,0,0,1,189,38.89,2,2,0,0,1,190.92,37ZM193,38V37.68l-1.67.25a0.88,0.88,0,0,0-.86.87,0.85,0.85,0,0,0,.94.83A1.46,1.46,0,0,0,193,38Zm3.35,2.66V34.21h1.46V35a2.19,2.19,0,0,1,1.9-1,2,2,0,0,1,1.92,1.13A2.25,2.25,0,0,1,203.75,34,2.2,2.2,0,0,1,206,36.47v4.16h-1.48V36.72a1.17,1.17,0,0,0-1.24-1.32A1.33,1.33,0,0,0,202,36.82v3.81h-1.5V36.72a1.17,1.17,0,0,0-1.24-1.32,1.31,1.31,0,0,0-1.33,1.42v3.81h-1.52Zm11.5,0V31.09h1.5V35a2.31,2.31,0,0,1,2-1c1.92,0,3,1.46,3,3.35s-1.15,3.4-3,3.4a2.2,2.2,0,0,1-2-1v0.86h-1.49Zm3.22-5.23a1.77,1.77,0,0,0-1.73,2,1.75,1.75,0,1,0,3.45,0A1.75,1.75,0,0,0,211.11,35.4ZM216.6,31a1,1,0,0,1,1,1A1,1,0,1,1,216.6,31Zm-0.75,9.67V34.21h1.52v6.42h-1.52Zm4.26-2.11a1.2,1.2,0,0,0,1.3,1.09,0.82,0.82,0,0,0,1-.74,0.77,0.77,0,0,0-.75-0.71L220.67,38a1.94,1.94,0,0,1-1.73-1.87A2.23,2.23,0,0,1,221.31,34a2.26,2.26,0,0,1,2.48,1.79l-1.29.37a1.1,1.1,0,0,0-1.19-1,0.83,0.83,0,0,0-.92.74,0.7,0.7,0,0,0,.65.67l1,0.2a2,2,0,0,1,1.85,2,2.18,2.18,0,0,1-2.43,2.06,2.41,2.41,0,0,1-2.65-1.94Zm7.2-4.31h1.33v1.36H227.3v3a0.7,0.7,0,0,0,.82.8,3.34,3.34,0,0,0,.53-0.05v1.27a2.41,2.41,0,0,1-.94.14,1.77,1.77,0,0,1-1.92-1.91V35.57H224.6V34.21h0.33a0.91,0.91,0,0,0,1-1v-1h1.38v2Zm8.51,4.59a2.89,2.89,0,0,1-2.9,2,3.41,3.41,0,0,1-.16-6.81,3,3,0,0,1,3.14,3.33,2.25,2.25,0,0,1,0,.49h-4.69a1.72,1.72,0,0,0,1.75,1.67,1.55,1.55,0,0,0,1.61-1.12Zm-1.46-2.08a1.44,1.44,0,0,0-1.58-1.42,1.54,1.54,0,0,0-1.57,1.42h3.15Z" transform="translate(-1 -1)"/></svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/fr-ca/services/">
                Nos services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link active" href="/fr-ca/company/">
                Notre entreprise
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/fr-ca/team/" class="secondary-nav-link">Équipe de gestion</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/compliance/" class="secondary-nav-link">Conformité <span class="soft-break"></span>&
                    Accréditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Carrières</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/fr-ca/faq/" class="secondary-nav-link active">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/fr-ca/blog/">
                Notre blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Canada <span>FR</span>
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australie</a>

                <a class="dropdown-item" href="https://firmafx.co.nz">Nouvelle-Zélande </a>

                <a class="dropdown-item" href="https://firmafx.co.uk">Royaume-Uni</a>

                <a class="dropdown-item" href="/en-us/">États-Unis</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="main-content container">

      <div class="row-sidebar">
        <div class="sidebar"></div>
        <div class="page-content">

          <section class="container light">

            <h1>
              Vous avez des questions <span class="soft-break"></span>
              et nous avons <span class="nobr">les meilleures réponses.</span>
            </h1>

            <ul class="faq-list">

              <li class="faq-item">
                <div class="question">Comment créer un compte?</div>
                <div class="answer">
                  Vous êtes à un appel d’un spécialiste de Firma. Il suffit d’appeler et ils créeront votre compte. Pour
                  obtenir les coordonnées dans votre région, référez-vous à la page
                  <a href="/fr-ca/contact-us/">Contactez-nous</a> sur notre <span class="nobr">site Web.</span>
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Est-ce qu’il y a un volume de transaction minimal pour chaque échange?</div>
                <div class="answer">
                  Nous offrons des solutions allant des petits aux grands volumes. Contactez un spécialiste de chez Firma
                  dès aujourd’hui pour discuter d’une solution qui convient à <span class="nobr">vos besoins.</span>
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Comment se protéger contre les fluctuations de devise?</div>
                <div class="answer">
                  Votre spécialiste Firma peut vous informer sur les mouvements du marché. Il ou elle peut également vous
                  donner de l’information sur des produits comme des contrats à terme qui peuvent vous protéger contre la
                  volatilité du marché, s’ils sont offerts dans votre région.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Est-ce possible de réserver un taux aujourd’hui et de le conserver?</div>
                <div class="answer">
                  Oui. Nous allons fixer le taux de change au moment de votre réservation et retenir ce taux pour vous, peu
                  importe quels mouvements prendront place dans le marché.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Est-ce possible d’acheter ou vendre des devises au taux que je choisis?</div>
                <div class="answer">
                  Oui. Dites à votre spécialiste le taux que vous préférez et vous serez contacté lorsqu’il sera atteint.

                </div>
              </li>
              <li class="faq-item">
                <div class="question">Est-ce que Firma s’occupe des contrats à terme et futurs?</div>
                <div class="answer">
                  Oui. FIRMA Corporation Cambiste est un leader de l’industrie des contrats à terme. Il n’y a pas de primes
                  pour ce service pour les transactions au comptant standard.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Qu’est-ce qui arrive si j’ai différentes devises à payer?</div>
                <div class="answer">
                  Envoyez-nous une liste des bénéficiaires et le montant à payer à chacun et nous nous en occuperons pour
                  vous en achetant en bloc afin de vous faire économiser de l’argent.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Que faire avec des créances en devises?</div>
                <div class="answer">
                  Firma peut recevoir vos virements en devises sans frais. Cela vous permettra de contrôler quand et à quel
                  taux vous changez de devise. Si vous êtes titulaire d’un compte en devises, laissez-le-nous savoir lorsque
                  vous recevrez les fonds et nous les transférerons dans la devise requise.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Est-ce que Firma peut effectuer un dépôt directement dans mon compte?</div>
                <div class="answer">
                  Oui. Firma peut déposer directement dans votre compte en utilisant plusieurs méthodes de dépôt. Certaines
                  sont mondiales et d’autres sont spécifiques à certains marchés. Peu importe où vous êtes, nous pouvons
                  élaborer une solution qui répondra à vos besoins.
                </div>
              </li>
              <li class="faq-item">
                <div class="question">Comment les transferts de Firma fonctionnent-ils?</div>
                <div class="answer">
                  Nous utilisons le même service international de transfert que toutes les institutions financières
                  importantes, mais à une fraction du prix. Les paiements sont totalement fiables et traçables.
                </div>
              </li>
            </ul>
          </section>

        </div>
      </div>

    </div>
	</div>
</asp:content>