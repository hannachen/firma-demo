<%@ Master Language="C#" MasterPageFile="~/masterpages/PageLayout-BasePage.master" AutoEventWireup="true" CodeBehind="JobDetailsPage.master.cs" Inherits="FirmaFx.Web.Masterpages.JobDetailsPage" %>
<%@ MasterType VirtualPath="~/Masterpages/PageLayout-BasePage.master" %>

<asp:content ContentPlaceHolderId="StylePlaceholder" runat="server">
	<link href="/content/fseditor.css" rel="stylesheet" type="text/css" media="all" />
</asp:content>

<asp:content ContentPlaceHolderId="ContentPlaceholder" runat="server">
	<umbraco:Macro Alias="JobDetails" runat="server" />
	<umbraco:Macro Alias="ResumeForm" runat="server"
        EmailFrom="noreply-web1@firmafx.com"
        EmailTo="tom.bolianatz@firmafx.com"
        EmailCC="[#jobEmail]"
        EmailSubject="Career Form Submission" />
</asp:content>