<%@ Master Language="C#" AutoEventWireup="true" CodeBehind="Root.master.cs" Inherits="FirmaFx.Web.Masterpages.Root" %><!doctype html>
<html lang="<umbraco:Item field="language" runat="server" recursive="true" stripParagraph="true" />">
<head runat="server">
    <meta charset="utf-8" />
	  <title><umbraco:Item field="pageTitle" htmlEncode="true" runat="server" /><umbraco:Macro Alias="siteTitle" runat="server" /></title>
    <umbraco:Item field="metaKeywords" runat="server" recursive="true" stripParagraph="true" insertTextBefore="<meta name='keywords' content='" insertTextAfter="' />" />
    <umbraco:Item field="metaDescription" runat="server" recursive="true" stripParagraph="true" insertTextBefore="<meta name='description' content='" insertTextAfter="' />" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msvalidate.01" content="8F016ECCDB472E03EA1F99C9577BC965" />
    <link rel="apple-touch-icon" sizes="57x57" href="/content/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/content/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/content/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/content/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/content/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/content/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/content/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/content/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/content/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/content/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/content/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="/content/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/content/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/content/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/content/favicons/manifest.json">
    <link rel="mask-icon" href="/content/favicons/safari-pinned-tab.svg" color="#633a94">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/content/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#633a94">
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <link href="/content/main.css" type="text/css" rel="stylesheet" />
    <asp:ContentPlaceHolder Id="RootStylePlaceholder" runat="server" />

    <script type="text/javascript">
      (function l(d){
        var site = '6901', page = 'generic_buttons', s, er = d.createElement('script');
        er.type = 'text/javascript';
        er.async = true;
        er.src = '//o2.eyereturn.com/?site=' + site + '&page=' + page;
        s = d.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(er, s);
      })(document);

      (function l(d){
        var site = '6901', page = 'generic', s, er = d.createElement('script');
        er.type = 'text/javascript';
        er.async = true;
        er.src = '//o2.eyereturn.com/?site=' + site + '&page=' + page;
        s = d.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(er, s);
      })(document);
    </script>
</head>

<body id="<umbraco:Item field="bodyID" htmlEncode="true" runat="server" />">

  <umbraco:Macro Alias="StaticBrowserUpgradeMessage" runat="server" />

  <asp:ContentPlaceHolder Id="RootContentPlaceholder" runat="server" />
  <asp:ContentPlaceHolder Id="RootAnalyticsPlaceholder" runat="server" />

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.5/TweenMax.min.js"></script>
  <script src="/scripts/vendor.js"></script>
  <script src="/scripts/plugins.js"></script>
  <script src="/scripts/main.js"></script>

  <!-- Go to www.addthis.com/dashboard to customize your tools -->
  <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57b5de7a6cfaa3a8"></script>

  <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='https://www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-19092405-5','auto'); ga('send','pageview');
  </script>

</body>
</html>