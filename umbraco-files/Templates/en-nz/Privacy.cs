<%@ Master Language="C#" MasterPageFile="~/masterpages/en-nz.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior nosidebar">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/" class="brand-link">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="234.4px" height="42.1px" viewBox="0 0 234.4 42.1" enable-background="new 0 0 234.4 42.1" preserveAspectRatio="xMinYMin meet"
                     shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo">
                    <polygon points="34.8,0 34.8,39.2 42.5,39.2 42.5,0 "/>
                    <path d="M73.9,23c7.5-4.1,7.2-11.9,7.2-11.9c0-10.9-11.9-11-11.9-11H49.6v39.1h7.6l0-14.7h9.2l8,14.7l8.9,0L73.9,23z
                       M73.6,12c0.1,5.1-6,5-6,5H57.4V7.6h11C74.1,7.7,73.6,12,73.6,12"/>
                    <path d="M27.6,7.6V0.1h-20C3.6,0.1,0.4,2.6,0,5.9c0,0.2,0,33.3,0,33.3h7.7l0-15.5l18.1,0V16l-18,0l0-8.4H27.6z"/>
                    <path d="M127.4,4.6c-0.1-2.5-2.2-4.6-4.7-4.6c-1.8,0-3.3,1-4.1,2.4l0,0l-10.6,18.8c0,0-10.5-18.4-10.5-18.6
                      C96.3,1,95,0,93.1,0c-2.6,0-4.7,2.1-4.7,4.7c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1v34.3H96l0-24.5l8.9,16.7h6l8.8-16.7v24.5h7.7
                      C127.4,39.3,127.4,4.7,127.4,4.6"/>
                    <path d="M155.5,2.5c-0.8-1.4-2.3-2.4-4-2.4c-1.8,0-3.3,1-4.1,2.5l-14.9,36.7h8.5l3.7-9.5h13.4l3.8,9.5h8.5L155.5,2.5
                      z M147.2,23.4l4.2-12.8l4.3,12.8H147.2z"/>
                    <polygon points="176.5,25.9 176.5,16.8 182,16.8 182,18.3 178.1,18.3 178.1,20.5 181.7,20.5 181.7,22 178.1,22
                      178.1,25.9 "/>
                    <path d="M182.7,22.6c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1c1,0,1.8,0.3,2.5,1c0.7,0.7,1,1.5,1,2.5
                      c0,1-0.4,1.8-1,2.5c-0.7,0.7-1.5,1-2.5,1c-1,0-1.8-0.3-2.5-1C183.1,24.4,182.7,23.6,182.7,22.6 M187.8,24.1c0.4-0.4,0.6-0.9,0.6-1.5
                      s-0.2-1.1-0.6-1.5c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1.1,0.2-1.5,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.5,0.6C186.9,24.6,187.4,24.5,187.8,24.1"/>
                    <path d="M191,25.9v-6.6h1.5v1c0.3-0.7,1-1.1,2-1.1c0.2,0,0.3,0,0.5,0v1.5c-0.2,0-0.4-0.1-0.7-0.1
                      c-1.1,0-1.8,0.7-1.8,1.8v3.5H191z"/>
                    <path d="M195.6,22.6c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C195.9,24.5,195.6,23.7,195.6,22.6L195.6,22.6z M200.7,21.9c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H200.7z"/>
                    <path d="M203.2,17.6c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
                      C203.6,18.5,203.2,18.1,203.2,17.6 M203.3,19.3h1.6v6.6h-1.6V19.3z"/>
                    <path d="M211.4,20.3v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7V25c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C210.3,19.2,211.1,19.6,211.4,20.3 M210.9,24
                      c0.4-0.4,0.6-0.9,0.6-1.4s-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4s0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C210,24.5,210.5,24.4,210.9,24"/>
                    <path d="M214.4,25.9v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1h-1.6V22c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H214.4z"/>
                    <polygon points="176.5,39.2 176.5,30.2 182.3,30.2 182.3,31.6 178.1,31.6 178.1,33.8 182,33.8 182,35.3 178.1,35.3
                      178.1,37.8 182.4,37.8 182.4,39.2 "/>
                    <polygon points="183.2,39.2 185.7,35.8 183.4,32.7 185.1,32.7 186.6,34.6 188,32.7 189.7,32.7 187.4,35.8
                      189.9,39.2 188.2,39.2 186.6,36.9 184.9,39.2 "/>
                    <path d="M196.3,33.6l-1,1c-0.5-0.5-1-0.7-1.7-0.7c-0.6,0-1.1,0.2-1.4,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.4,0.6c0.7,0,1.2-0.2,1.7-0.7l1,1c-0.7,0.8-1.6,1.1-2.6,1.1c-1,0-1.8-0.3-2.5-1c-0.7-0.7-1-1.5-1-2.5
                      s0.3-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1C194.7,32.5,195.6,32.9,196.3,33.6"/>
                    <path d="M197.2,39.2v-9.1h1.5v3.4c0.3-0.6,1-1,1.9-1c1.5,0,2.4,1,2.4,2.6v4.1h-1.6v-3.8c0-0.9-0.5-1.5-1.3-1.5
                      c-0.8,0-1.4,0.6-1.4,1.5v3.8H197.2z"/>
                    <path d="M209.5,33.6v-1h1.6v6.6h-1.6v-1c-0.2,0.6-1,1.1-2.1,1.1c-0.9,0-1.7-0.3-2.3-1c-0.6-0.7-0.9-1.5-0.9-2.4
                      s0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C208.5,32.5,209.3,33,209.5,33.6 M209,37.4c0.4-0.4,0.6-0.9,0.6-1.4c0-0.6-0.2-1.1-0.6-1.4
                      c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.9-0.5,1.4c0,0.6,0.2,1.1,0.5,1.4c0.4,0.4,0.8,0.6,1.4,0.6
                      C208.2,38,208.7,37.8,209,37.4"/>
                    <path d="M212.5,39.2v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1H217v-3.8c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H212.5z"/>
                    <path d="M225,33.6v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7v-0.3c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,1-2.4c0.6-0.7,1.4-1,2.3-1C224,32.5,224.8,33,225,33.6 M224.5,37.4c0.4-0.4,0.6-0.9,0.6-1.4
                      c0-0.6-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4c0,0.6,0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C223.7,37.9,224.2,37.7,224.5,37.4"/>
                    <path d="M227.7,36c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C228,37.9,227.7,37,227.7,36L227.7,36z M232.8,35.3c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H232.8z"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/services/">
                Our Services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link" href="/company/">
                Our Company
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/team/" class="secondary-nav-link">Management Team</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/compliance/" class="secondary-nav-link">Compliance <span class="soft-break"></span>and
                    Accreditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Careers</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/blog/">
                Blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                New Zealand
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="https://firmafx.ca/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.ca/fr-ca/">Canada <span>FR</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australia</a>

                <a class="dropdown-item" href="https://firmafx.co.uk">United Kingdom</a>

                <a class="dropdown-item" href="https://firmafx.com/en-us/">United States</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="main-content container">

      <div class="keyline"></div>
      <div class="page-content">

        <section class="container light">

          <h1>Privacy Policy</h1>

          <p>FIRMA Foreign Exchange is committed to safeguarding the personal and confidential information entrusted to us
            by our valued clients. Personal and confidential information means information about an identifiable individual
            or company; this can include an individual or company’s name, address, phone number, identifying number,
            financial information, etc.</p>


          <h2>Collection of Personal and Confidential Information</h2>

          <p>FIRMA Foreign Exchange collects only the information needed for the purpose of providing superior service to
            clients. This includes information needed to:</p>

          <ul>
            <li>Establish a personal one-to-one relationship with each client</li>
            <li>Efficiently deliver (either in person or electronically) requested products and services</li>
            <li>Provide the appropriate and specific market information to meet each client’s needs</li>
            <li>Follow up with clients to determine satisfaction with products and services</li>
            <li>Notify clients of upcoming events of interest</li>
            <li>Grant credit</li>
            <li>Meet regulatory requirements</li>
          </ul>

          <p>FIRMA Foreign Exchange normally collects client information directly from clients. FIRMA Foreign Exchange may
            collect client information from other persons with the client's consent or as authorized by law.</p>

          <p>FIRMA Foreign Exchange informs clients of the purposes for which their information is being collected, before
            or at the time of collecting personal information. The exception to this is when a client volunteers information
            for an obvious purpose (in this case the client’s consent is assumed) or in specific circumstances where
            collection, use or disclosure without consent is authorized or required by law.</p>

          <p>FIRMA Foreign Exchange may ask for a client’s express consent for some purposes but may not be able to provide
            certain services if the client is unwilling to provide consent to the collection, use or disclosure of certain
            information. Where express consent is needed, FIRMA Foreign Exchange will normally ask clients to provide their
            consent orally (i.e. in person, by telephone), in writing (i.e. by signing a consent form, by checking a box on
            a form), or electronically (i.e. by clicking a button).</p>

          <p>A client may withdraw consent to the use and disclosure of personal information at any time, unless the
            personal information is necessary for FIRMA Foreign Exchange to fulfill legal obligations. FIRMA Foreign
            Exchange will respect a client’s decision but may not be able to provide the client with certain products and
            services if we do not have the necessary personal information.</p>

          <p>FIRMA Foreign Exchange may collect, use or disclose client personal and confidential information without
            consent only as authorized by law. For example, FIRMA Foreign Exchange may not request consent when the
            collection, use or disclosure is reasonable for an investigation or legal proceeding; when collecting a debt
            owed to the organization; in an emergency that threatens life, health or safety, or when the personal
            information is from a public telephone directory.</p>


          <h2>Use of Confidential Information</h2>

          <p>FIRMA Foreign Exchange uses and discloses client information only for the purposes for which the information
            was collected, except as authorized by law. For example, FIRMA Foreign Exchange may use client contact
            information to ensure that the client receives the best possible service. The law also allows FIRMA Foreign
            Exchange to use that contact information for the purpose of collecting a debt owed to the organization, should
            that be necessary.</p>

          <p>FIRMA Foreign Exchange makes every reasonable effort to ensure that client information is accurate and
            complete. FIRMA Foreign Exchange relies on clients to provide notification if there is a change to their
            information that may affect their relationship with FIRMA Foreign Exchange. In some cases FIRMA Foreign Exchange
            may ask for a written request for correction.</p>


          <h2>Security</h2>

          <p>FIRMA Foreign Exchange protects client information in a manner appropriate for the sensitivity of the
            information and as required by applicable legislation. Client information is stored in electronic format under a
            strict secure computer system and may not be accessed by anyone outside of FIRMA Foreign Exchange’s employment.
            Documents in paper format are stored in a secure storage facility and retained as long as required by law. FIRMA
            Foreign Exchange makes every reasonable effort to prevent any loss, misuse, disclosure or modification of
            personal information, as well as any unauthorized access to personal information. Maintaining the security of
            personal and confidential information is of paramount importance to FIRMA Foreign Exchange.</p>

          <p>FIRMA Foreign Exchange uses appropriate security measures when destroying client information, including
            shredding paper records and permanently deleting electronic records. FIRMA Foreign Exchange retains client
            personal information only as long as is reasonable to fulfill the purposes for which the information was
            collected or for legal or business purposes.</p>


          <h2>Rights of Access</h2>

          <p>Clients of FIRMA Foreign Exchange have a Right of Access to their own personal information in a record that is
            in the custody or under the control of FIRMA Foreign Exchange, subject to some exceptions. For example, FIRMA
            Foreign Exchange may refuse to provide access to information that would reveal personal information about
            another individual or company. Access may also be refused if the information is privileged or contained in
            mediation records.</p>

          <p>If FIRMA Foreign Exchange refuses a request in whole or in part, FIRMA Foreign Exchange will provide the
            reasons for the refusal. In some cases, where exceptions to access apply, FIRMA Foreign Exchange may withhold
            that information and provide the client with the remainder of the record.</p>

          <p>While FIRMA Foreign Exchange strives to respond to each request as quickly as possible, information retrieval
            may take up to a maximum of 45 days. FIRMA Foreign Exchange may charge a reasonable fee to provide information,
            but not to make a correction. FIRMA Foreign Exchange will advise you of any fees that may apply before beginning
            to process your request.</p>

          <p>If you have a question or concern about any collection, use or disclosure of personal information by FIRMA
            Foreign Exchange, or about a request for access to your own personal information, please contact your local
            trading office.</p>

        </section>

      </div>

    </div>
	</div>
</asp:content>