<%@ Master Language="C#" MasterPageFile="~/masterpages/en-nz.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/" class="brand-link">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="234.4px" height="42.1px" viewBox="0 0 234.4 42.1" enable-background="new 0 0 234.4 42.1" preserveAspectRatio="xMinYMin meet"
                     shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo">
                    <polygon points="34.8,0 34.8,39.2 42.5,39.2 42.5,0 "/>
                    <path d="M73.9,23c7.5-4.1,7.2-11.9,7.2-11.9c0-10.9-11.9-11-11.9-11H49.6v39.1h7.6l0-14.7h9.2l8,14.7l8.9,0L73.9,23z
                       M73.6,12c0.1,5.1-6,5-6,5H57.4V7.6h11C74.1,7.7,73.6,12,73.6,12"/>
                    <path d="M27.6,7.6V0.1h-20C3.6,0.1,0.4,2.6,0,5.9c0,0.2,0,33.3,0,33.3h7.7l0-15.5l18.1,0V16l-18,0l0-8.4H27.6z"/>
                    <path d="M127.4,4.6c-0.1-2.5-2.2-4.6-4.7-4.6c-1.8,0-3.3,1-4.1,2.4l0,0l-10.6,18.8c0,0-10.5-18.4-10.5-18.6
                      C96.3,1,95,0,93.1,0c-2.6,0-4.7,2.1-4.7,4.7c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1v34.3H96l0-24.5l8.9,16.7h6l8.8-16.7v24.5h7.7
                      C127.4,39.3,127.4,4.7,127.4,4.6"/>
                    <path d="M155.5,2.5c-0.8-1.4-2.3-2.4-4-2.4c-1.8,0-3.3,1-4.1,2.5l-14.9,36.7h8.5l3.7-9.5h13.4l3.8,9.5h8.5L155.5,2.5
                      z M147.2,23.4l4.2-12.8l4.3,12.8H147.2z"/>
                    <polygon points="176.5,25.9 176.5,16.8 182,16.8 182,18.3 178.1,18.3 178.1,20.5 181.7,20.5 181.7,22 178.1,22
                      178.1,25.9 "/>
                    <path d="M182.7,22.6c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1c1,0,1.8,0.3,2.5,1c0.7,0.7,1,1.5,1,2.5
                      c0,1-0.4,1.8-1,2.5c-0.7,0.7-1.5,1-2.5,1c-1,0-1.8-0.3-2.5-1C183.1,24.4,182.7,23.6,182.7,22.6 M187.8,24.1c0.4-0.4,0.6-0.9,0.6-1.5
                      s-0.2-1.1-0.6-1.5c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1.1,0.2-1.5,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.5,0.6C186.9,24.6,187.4,24.5,187.8,24.1"/>
                    <path d="M191,25.9v-6.6h1.5v1c0.3-0.7,1-1.1,2-1.1c0.2,0,0.3,0,0.5,0v1.5c-0.2,0-0.4-0.1-0.7-0.1
                      c-1.1,0-1.8,0.7-1.8,1.8v3.5H191z"/>
                    <path d="M195.6,22.6c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C195.9,24.5,195.6,23.7,195.6,22.6L195.6,22.6z M200.7,21.9c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H200.7z"/>
                    <path d="M203.2,17.6c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
                      C203.6,18.5,203.2,18.1,203.2,17.6 M203.3,19.3h1.6v6.6h-1.6V19.3z"/>
                    <path d="M211.4,20.3v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7V25c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C210.3,19.2,211.1,19.6,211.4,20.3 M210.9,24
                      c0.4-0.4,0.6-0.9,0.6-1.4s-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4s0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C210,24.5,210.5,24.4,210.9,24"/>
                    <path d="M214.4,25.9v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1h-1.6V22c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H214.4z"/>
                    <polygon points="176.5,39.2 176.5,30.2 182.3,30.2 182.3,31.6 178.1,31.6 178.1,33.8 182,33.8 182,35.3 178.1,35.3
                      178.1,37.8 182.4,37.8 182.4,39.2 "/>
                    <polygon points="183.2,39.2 185.7,35.8 183.4,32.7 185.1,32.7 186.6,34.6 188,32.7 189.7,32.7 187.4,35.8
                      189.9,39.2 188.2,39.2 186.6,36.9 184.9,39.2 "/>
                    <path d="M196.3,33.6l-1,1c-0.5-0.5-1-0.7-1.7-0.7c-0.6,0-1.1,0.2-1.4,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.4,0.6c0.7,0,1.2-0.2,1.7-0.7l1,1c-0.7,0.8-1.6,1.1-2.6,1.1c-1,0-1.8-0.3-2.5-1c-0.7-0.7-1-1.5-1-2.5
                      s0.3-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1C194.7,32.5,195.6,32.9,196.3,33.6"/>
                    <path d="M197.2,39.2v-9.1h1.5v3.4c0.3-0.6,1-1,1.9-1c1.5,0,2.4,1,2.4,2.6v4.1h-1.6v-3.8c0-0.9-0.5-1.5-1.3-1.5
                      c-0.8,0-1.4,0.6-1.4,1.5v3.8H197.2z"/>
                    <path d="M209.5,33.6v-1h1.6v6.6h-1.6v-1c-0.2,0.6-1,1.1-2.1,1.1c-0.9,0-1.7-0.3-2.3-1c-0.6-0.7-0.9-1.5-0.9-2.4
                      s0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C208.5,32.5,209.3,33,209.5,33.6 M209,37.4c0.4-0.4,0.6-0.9,0.6-1.4c0-0.6-0.2-1.1-0.6-1.4
                      c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.9-0.5,1.4c0,0.6,0.2,1.1,0.5,1.4c0.4,0.4,0.8,0.6,1.4,0.6
                      C208.2,38,208.7,37.8,209,37.4"/>
                    <path d="M212.5,39.2v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1H217v-3.8c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H212.5z"/>
                    <path d="M225,33.6v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7v-0.3c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,1-2.4c0.6-0.7,1.4-1,2.3-1C224,32.5,224.8,33,225,33.6 M224.5,37.4c0.4-0.4,0.6-0.9,0.6-1.4
                      c0-0.6-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4c0,0.6,0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C223.7,37.9,224.2,37.7,224.5,37.4"/>
                    <path d="M227.7,36c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C228,37.9,227.7,37,227.7,36L227.7,36z M232.8,35.3c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H232.8z"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/services/">
                Our Services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link active" href="/company/">
                Our Company
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/team/" class="secondary-nav-link">Management Team</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/compliance/" class="secondary-nav-link active">Compliance <span class="soft-break"></span>and
                    Accreditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Careers</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/blog/">
                Blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                New Zealand
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="https://firmafx.ca/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.ca/fr-ca/">Canada <span>FR</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australia</a>

                <a class="dropdown-item" href="https://firmafx.co.uk">United Kingdom</a>

                <a class="dropdown-item" href="https://firmafx.com/en-us/">United States</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="main-content container">

      <div class="row-sidebar">
        <div class="sidebar"></div>
        <div class="page-content">

          <section class="hero hero-secondary" style="background-image:url(/images/company/background-compliance.jpg)" ;>
            <div class="contents container">
              <div class="text">
                <div class="firma-equals">
                  <img src="/images/text-firma-equels.svg" class="text-firma-equals">
                  <div class="handwriting">
                    <div class="sprite diligence"></div>

                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="container light">

            <h1>Premium service requires <span class="soft-break"></span>premium security.</h1>
            <p>Firma believes the best way to do business is to be personable, straightforward and dependable.</p>
            <p>Our entire company employs strict trading and settlement procedures that incorporate risk-based methodologies
              to detect, prevent and report suspicious illicit activity. We verify the identity of every client and ensure
              corporate records are maintained in accordance with prevailing legislation.</p>
            <p>Firma is committed to maintaining a strong anti-money laundering and anti-terrorist financing program. Our
              company undertakes regular external reviews and risk assessments in all our global locations to assess and
              improve our ability to combat money laundering and terrorist financing.</p>
            <p>Our Compliance Department continually monitors jurisdictional legislation to ensure our compliance program
              meets international requirements. The department leverages leading edge transaction monitoring technology to
              enhance the security of funds flowing through our systems. The compliance team continually improves and
              refreshes the Firma compliance program through ongoing internal reviews and regular, professional training of
              our personnel</p>
            <p>Firma only provides general financial advice. General advice means that Firma does not take into account your
              objectives, financial situation, or needs. You should review your situation to decide yourself whether our
              products are appropriate for you. For more information regarding our products and financial services, please
          consult our <a href="https://firmafx.com.au/media/13900/firma_fsg_pds_au_web.pdf" target="_blank">Product
            Disclosure Statement</a>.</p>
          </section>

          <section class="container light">

            <h1>Because we’re vetted by the best.</h1>
            <p>Firma annually undertakes audits of its consolidated financial process and statements completed by
              PricewaterhouseCoopers LLP.</p>
            <p>Firma maintains a financial institution bond that includes broad coverage against losses resulting from
              theft, burglary, and/or disappearance of money and securities. We use Lloyd Sadd Insurance Brokers Ltd. as our
              broker and QBE Services Inc. underwritten by Lloyd's of London as our insurer.</p>

            <h2>Certificates of Insurance</h2>
            <ul class="certificates">
              <li>
            <a href="https://firmafx.ca/media/40188/firma_16-17_bond_cert_-_can.pdf" target="_blank">Canada</a>
              </li>
              <li>
            <a href="https://firmafx.ca/media/40194/firma_16-17_bond_cert_-_uk.PDF" target="_blank">United Kingdom</a>
              </li>
              <li>
            <a href="https://firmafx.ca/media/40191/firma_16-17_bond_cert_-_nz.PDFf" target="_blank">New Zealand</a>
              </li>
              <li>
            <a href="https://firmafx.ca/media/40185/firma_16-17_bond_cert_-_aus.PDF" target="_blank">Australia</a>
              </li>
              <li>
            <a href="https://firmafx.ca/media/40197/firma_16-17_bond_cert_-_us.PDF" target="_blank">United States</a>
              </li>
            </ul>
          </section>

        </div>
      </div>

    </div>
	</div>
</asp:content>