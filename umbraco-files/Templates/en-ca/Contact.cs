<%@ Master Language="C#" MasterPageFile="~/masterpages/en-ca.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">

  <div class="interior nosidebar">

    <div id="header">
      <input type="hidden" autofocus="true" />
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/en-ca/" class="brand-link">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
               width="234.4px" height="42.1px" viewBox="0 0 234.4 42.1" enable-background="new 0 0 234.4 42.1" preserveAspectRatio="xMinYMin meet"
               shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo">
              <polygon points="34.8,0 34.8,39.2 42.5,39.2 42.5,0 "/>
              <path d="M73.9,23c7.5-4.1,7.2-11.9,7.2-11.9c0-10.9-11.9-11-11.9-11H49.6v39.1h7.6l0-14.7h9.2l8,14.7l8.9,0L73.9,23z
                 M73.6,12c0.1,5.1-6,5-6,5H57.4V7.6h11C74.1,7.7,73.6,12,73.6,12"/>
              <path d="M27.6,7.6V0.1h-20C3.6,0.1,0.4,2.6,0,5.9c0,0.2,0,33.3,0,33.3h7.7l0-15.5l18.1,0V16l-18,0l0-8.4H27.6z"/>
              <path d="M127.4,4.6c-0.1-2.5-2.2-4.6-4.7-4.6c-1.8,0-3.3,1-4.1,2.4l0,0l-10.6,18.8c0,0-10.5-18.4-10.5-18.6
                C96.3,1,95,0,93.1,0c-2.6,0-4.7,2.1-4.7,4.7c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1v34.3H96l0-24.5l8.9,16.7h6l8.8-16.7v24.5h7.7
                C127.4,39.3,127.4,4.7,127.4,4.6"/>
              <path d="M155.5,2.5c-0.8-1.4-2.3-2.4-4-2.4c-1.8,0-3.3,1-4.1,2.5l-14.9,36.7h8.5l3.7-9.5h13.4l3.8,9.5h8.5L155.5,2.5
                z M147.2,23.4l4.2-12.8l4.3,12.8H147.2z"/>
              <polygon points="176.5,25.9 176.5,16.8 182,16.8 182,18.3 178.1,18.3 178.1,20.5 181.7,20.5 181.7,22 178.1,22
                178.1,25.9 "/>
              <path d="M182.7,22.6c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1c1,0,1.8,0.3,2.5,1c0.7,0.7,1,1.5,1,2.5
                c0,1-0.4,1.8-1,2.5c-0.7,0.7-1.5,1-2.5,1c-1,0-1.8-0.3-2.5-1C183.1,24.4,182.7,23.6,182.7,22.6 M187.8,24.1c0.4-0.4,0.6-0.9,0.6-1.5
                s-0.2-1.1-0.6-1.5c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1.1,0.2-1.5,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                c0.4,0.4,0.9,0.6,1.5,0.6C186.9,24.6,187.4,24.5,187.8,24.1"/>
              <path d="M191,25.9v-6.6h1.5v1c0.3-0.7,1-1.1,2-1.1c0.2,0,0.3,0,0.5,0v1.5c-0.2,0-0.4-0.1-0.7-0.1
                c-1.1,0-1.8,0.7-1.8,1.8v3.5H191z"/>
              <path d="M195.6,22.6c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                C195.9,24.5,195.6,23.7,195.6,22.6L195.6,22.6z M200.7,21.9c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                c-0.4,0.3-0.5,0.6-0.6,1H200.7z"/>
              <path d="M203.2,17.6c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
                C203.6,18.5,203.2,18.1,203.2,17.6 M203.3,19.3h1.6v6.6h-1.6V19.3z"/>
              <path d="M211.4,20.3v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7V25c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C210.3,19.2,211.1,19.6,211.4,20.3 M210.9,24
                c0.4-0.4,0.6-0.9,0.6-1.4s-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4s0.2,1,0.5,1.4
                c0.4,0.4,0.8,0.6,1.4,0.6C210,24.5,210.5,24.4,210.9,24"/>
              <path d="M214.4,25.9v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1h-1.6V22c0-0.9-0.5-1.5-1.4-1.5
                c-0.9,0-1.5,0.6-1.5,1.5v3.8H214.4z"/>
              <polygon points="176.5,39.2 176.5,30.2 182.3,30.2 182.3,31.6 178.1,31.6 178.1,33.8 182,33.8 182,35.3 178.1,35.3
                178.1,37.8 182.4,37.8 182.4,39.2 "/>
              <polygon points="183.2,39.2 185.7,35.8 183.4,32.7 185.1,32.7 186.6,34.6 188,32.7 189.7,32.7 187.4,35.8
                189.9,39.2 188.2,39.2 186.6,36.9 184.9,39.2 "/>
              <path d="M196.3,33.6l-1,1c-0.5-0.5-1-0.7-1.7-0.7c-0.6,0-1.1,0.2-1.4,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                c0.4,0.4,0.9,0.6,1.4,0.6c0.7,0,1.2-0.2,1.7-0.7l1,1c-0.7,0.8-1.6,1.1-2.6,1.1c-1,0-1.8-0.3-2.5-1c-0.7-0.7-1-1.5-1-2.5
                s0.3-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1C194.7,32.5,195.6,32.9,196.3,33.6"/>
              <path d="M197.2,39.2v-9.1h1.5v3.4c0.3-0.6,1-1,1.9-1c1.5,0,2.4,1,2.4,2.6v4.1h-1.6v-3.8c0-0.9-0.5-1.5-1.3-1.5
                c-0.8,0-1.4,0.6-1.4,1.5v3.8H197.2z"/>
              <path d="M209.5,33.6v-1h1.6v6.6h-1.6v-1c-0.2,0.6-1,1.1-2.1,1.1c-0.9,0-1.7-0.3-2.3-1c-0.6-0.7-0.9-1.5-0.9-2.4
                s0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C208.5,32.5,209.3,33,209.5,33.6 M209,37.4c0.4-0.4,0.6-0.9,0.6-1.4c0-0.6-0.2-1.1-0.6-1.4
                c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.9-0.5,1.4c0,0.6,0.2,1.1,0.5,1.4c0.4,0.4,0.8,0.6,1.4,0.6
                C208.2,38,208.7,37.8,209,37.4"/>
              <path d="M212.5,39.2v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1H217v-3.8c0-0.9-0.5-1.5-1.4-1.5
                c-0.9,0-1.5,0.6-1.5,1.5v3.8H212.5z"/>
              <path d="M225,33.6v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7v-0.3c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,1-2.4c0.6-0.7,1.4-1,2.3-1C224,32.5,224.8,33,225,33.6 M224.5,37.4c0.4-0.4,0.6-0.9,0.6-1.4
                c0-0.6-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4c0,0.6,0.2,1,0.5,1.4
                c0.4,0.4,0.8,0.6,1.4,0.6C223.7,37.9,224.2,37.7,224.5,37.4"/>
              <path d="M227.7,36c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                C228,37.9,227.7,37,227.7,36L227.7,36z M232.8,35.3c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                c-0.4,0.3-0.5,0.6-0.6,1H232.8z"/>
            </svg>      </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
            <ul class="nav navbar-nav">
              <li class="spacer"></li>
              <li class="nav-item">
                <a class="nav-link" href="/en-ca/services/">
                  Our Services
                  <i class="icon icon-chevron-right" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav-item our-company">
                <a class="nav-link" href="/en-ca/company/">
                  Our Company
                  <i class="icon icon-chevron-right" aria-hidden="true"></i>
                </a>
                <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                  <li class="secondary-nav-item">
                    <a href="/en-ca/team/" class="secondary-nav-link">Management Team</a>
                  </li>
                  <li class="secondary-nav-item">
                    <a href="/en-ca/compliance/" class="secondary-nav-link">Compliance <span class="soft-break"></span>and Accreditations</a>
                  </li>
                  <li class="secondary-nav-item">
                    <a href="https://careers.firmafx.com" class="secondary-nav-link">Careers</a>
                  </li>
                  <li class="secondary-nav-item">
                    <a href="/en-ca/faq/" class="secondary-nav-link">FAQs</a>
                  </li>
                </ul>
              </li>
              <li class="nav-item blog">
                <a class="nav-link" href="/en-ca/blog/">
                  Blog
                  <i class="icon icon-chevron-right" aria-hidden="true"></i>
                </a>
              </li>
              <li class="nav-item dropdown language-selector">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Canada <span>EN</span>
                  </a>
                <div class="dropdown-menu">

                    <a class="dropdown-item" href="/fr-ca/">Canada <span>FR</span></a>

                    <a class="dropdown-item" href="https://firmafx.com.au">Australia</a>

                    <a class="dropdown-item" href="https://firmafx.co.nz">New Zealand</a>

                    <a class="dropdown-item" href="https://firmafx.co.uk">United Kingdom</a>

                    <a class="dropdown-item" href="/en-us/">United States</a>

                </div>
              </li>
            </ul>    </nav>
      </div>
    </div>

  <form runat="server">
    <asp:ScriptManager runat="server">
        <Scripts>
            <asp:ScriptReference Name="MsAjaxBundle" />
            <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
            <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
            <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
            <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
            <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
            <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
            <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
            <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
            <asp:ScriptReference Name="WebFormsBundle" />
        </Scripts>
    </asp:ScriptManager>
    <!--
    <umbraco:Macro Alias="ContactForm" runat="server"
                   CountryFilter="[#websiteLocationID]"
                   EmailFrom="[#emailFrom]"
                   EmailSupportTo="[#emailFormTo]"
                   EmailWebmaster="[#emailWebmaster]"
                   EmailCC="[#emailFormCC]"
                   SupportSubject="[#supportSubject]"
                   SupportContent="[#supportContent]"
                   SalesSubject="[#salesSubject]"
                   SalesContent="[#salesContent]"
                   ProblemSubject="[#problemSubject]"
                   ProblemContent="[#problemContent]"
                   RedirectID="[#redirectID]" />
                   -->
  </form>

  <div class="main-content container">

    <div class="keyline"></div>
    <div class="page-content">

      <section class="container light">

        <h1>Contact Us</h1>

        <div class="contact-row">

          <div class="col office-contact-container">
          <h2>
            Address and Phone
          </h2>

          <div class="office-contact">
            <ul class="contact-info">


              <li class="location collapse" id="AB0">
                <h3 class="office-name">
                  Edmonton
                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp; Corporate Division
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Suite 400, 10205 101 Street</span><br/>
                    <span itemprop="addressLocality">Edmonton, </span>
                    <span itemprop="addressRegion">Alberta</span>
                    <span itemprop="postalCode">T5J 4H5</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 426-2605</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (780) 426-5971</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 426-5920</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (780) 426-5920</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse in" id="AB2">
                <h3 class="office-name">


                  Head Office

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp; Head Office
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Suite 400, 10205 101 Street NW</span><br/>
                    <span itemprop="addressLocality">Edmonton, </span>
                    <span itemprop="addressRegion">Alberta</span>
                    <span itemprop="postalCode">T5J 4H5</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (877) 376-4946</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (780) 426-4946</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (877) 257-3847</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (877) 702-3847</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="AB3">
                <h3 class="office-name">


                  Calgary

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Life Plaza Building, 300 - 734 7 Ave SW</span><br/>
                    <span itemprop="addressLocality">Calgary, </span>
                    <span itemprop="addressRegion">Alberta</span>
                    <span itemprop="postalCode">T2P 3P8</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 262-2361</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (403) 262-2361</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 262-5111</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (403) 262-2418</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="BC0">
                <h3 class="office-name">


                  Vancouver

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">584-885 Dunsmuir St.</span><br/>
                    <span itemprop="addressLocality">Vancouver, </span>
                    <span itemprop="addressRegion">British Columbia</span>
                    <span itemprop="postalCode">V6C 1N5</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 822-8818</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (604) 682-8818</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (877) 682-8857</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (604) 682-8857</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="BC1">
                <h3 class="office-name">


                  Surrey

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 822-8818</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (877) 682-8857</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="BC2">
                <h3 class="office-name">


                  Kelowna

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 262-2361</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 262-5111</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="BC3">
                <h3 class="office-name">


                  Victoria

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 822-8818</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (877) 682-8857</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="SK0">
                <h3 class="office-name">


                  Saskatoon

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 445-6239</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 502-3310</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="MB0">
                <h3 class="office-name">


                  Winnipeg

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">309-93 Lombard Ave</span><br/>
                    <span itemprop="addressLocality">Winnipeg, </span>
                    <span itemprop="addressRegion">Manitoba</span>
                    <span itemprop="postalCode">R3B 3B1</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 445-6239</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (204) 943-9445</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 502-3310</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (204) 943-7515</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON0">
                <h3 class="office-name">


                  Ottawa

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Burnside Building, 100-151 Slater St</span><br/>
                    <span itemprop="addressLocality">Ottawa, </span>
                    <span itemprop="addressRegion">Ontario</span>
                    <span itemprop="postalCode">K1P 5H3</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (613) 232-7038</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (613) 232-6392</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON1">
                <h3 class="office-name">


                  Toronto

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Suite 800, 330 Bay Street</span><br/>
                    <span itemprop="addressLocality">Toronto, </span>
                    <span itemprop="addressRegion">Ontario</span>
                    <span itemprop="postalCode">M5H 2S8</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (877) 737-7522</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (416) 363-1583</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 992-9001</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (416) 363-6560</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON2">
                <h3 class="office-name">


                  Barrie

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (877) 737-7522</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 992-9001</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON3">
                <h3 class="office-name">


                  Hamilton

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON4">
                <h3 class="office-name">


                  Kitchener

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 445-6239</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 502-3310</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON5">
                <h3 class="office-name">


                  London

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 445-6239</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 502-3310</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="ON6">
                <h3 class="office-name">


                  St. Catharines

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="QC0">
                <h3 class="office-name">


                  Montréal

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>

                  <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="streetAddress">Suite 600, 1260 Robert-Bourassa Blvd.</span><br/>
                    <span itemprop="addressLocality">Montréal, </span>
                    <span itemprop="addressRegion">Quebec</span>
                    <span itemprop="postalCode">H3B 3B9</span>
                    <br/><span itemprop="addressCountry">Canada</span>
                  </address>

                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 353-9555</span>
                    (toll free)

                    <br/>
                    <abbr title="Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (514) 868-9555</span>


                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 707-9666</span> (toll free)<br/>
                    <abbr title="Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (514) 868-9666</span><br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="QC1">
                <h3 class="office-name">


                  Québec City

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 353-9555</span>
                    (toll free)

                    <br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="NB0">
                <h3 class="office-name">


                  New Brunswick

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="PE0">
                <h3 class="office-name">


                  Prince Edward Island

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="NS0">
                <h3 class="office-name">


                  Dartmouth

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="NL0">
                <h3 class="office-name">


                  Newfoundland

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 375-2124</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 513-1405</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="NV0">
                <h3 class="office-name">


                  Nunavut

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 426-2605</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 426-5920</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="NT0">
                <h3 class="office-name">


                  Northwest Territories

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 426-2605</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 426-5920</span> (toll free)<br/>


                  </div>

                </div>
              </li>


              <li class="location collapse" id="YK0">
                <h3 class="office-name">


                  Yukon

                </h3>

                <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                  <div class="office-name">
                    <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                  </div>


                  <div class="contact-details">
                    <abbr title="Toll-Free Telephone">T:</abbr><!--
                                    --><span itemprop="telephone">1 (866) 426-2605</span>
                    (toll free)

                    <br/>

                    <abbr title="Toll-Free Fax">F:</abbr><!--
                                    --><span itemprop="faxNumber">1 (866) 426-5920</span> (toll free)<br/>


                  </div>

                </div>
              </li>

            </ul>
          </div>

          <button class="btn btn-default dropdown-toggle btn-block" id="office-dropdown" type="button" data-toggle="dropdown" aria-expanded="true">
            Additional Locations
            <span class="caret"></span>
          </button>

          <ul class="office-contact-list dropdown-menu" role="menu" aria-labelledby="office-dropdown">

            <li role="presentation" class="dropdown-header location-item">Alberta</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#AB0">

                Edmonton
              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#AB2">


                Head Office
              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#AB3">


                Calgary

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">British Columbia</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#BC0">


                Vancouver

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#BC1">


                Surrey

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#BC2">


                Kelowna

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#BC3">


                Victoria

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Saskatchewan</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#SK0">


                Saskatoon

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Manitoba</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#MB0">


                Winnipeg

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Ontario</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON0">


                Ottawa

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON1">


                Toronto

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON2">


                Barrie

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON3">


                Hamilton

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON4">


                Kitchener

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON5">


                London

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#ON6">


                St. Catharines

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Quebec</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#QC0">


                Montréal

              </a>
            </li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#QC1">


                Québec City

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">New Brunswick</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NB0">


                New Brunswick

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Prince Edward Island</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#PE0">


                Prince Edward Island

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Nova Scotia</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NS0">


                Dartmouth

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Newfoundland</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NL0">


                Newfoundland

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Nunavut</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NV0">


                Nunavut

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Northwest Territories</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NT0">


                Northwest Territories

              </a>
            </li>


            <li role="presentation" class="dropdown-header location-item">Yukon</li>


            <li role="presentation" class="location-item city">
              <a role="menuitem" tabindex="-1" class="office-contact-link" href="#YK0">


                Yukon

              </a>
            </li>


          </ul>
        </div>
          <div class="col contact-form-container">
            <h2>Learn how Firma can help <span class="nobr">your business.</span></h2>
            <form action="//go.pardot.com/l/108482/2016-09-14/cc9jt?" method="post" class="contact-form" id="contact-page-form" data-parsley-trigger="keyup blur">
              <div class="field first">
                <input name="first" type="text" class="textinput" autocomplete="off" placeholder="First name" maxlength="20"
                       data-parsley-length="[2, 20]" data-parsley-length-message="Name should be between 2 and 20 characters" required=""/>
              </div>
              <div class="field email">
                <input name="email" type="email" class="textinput" autocomplete="off" placeholder="Your email address" maxlength="40"
                       data-parsley-length="[8, 40]" data-parsley-type-message="Invalid email address" required=""/>
              </div>
              <div class="button">
                <button type="submit" class="submit" disabled>Submit</button>
              </div>
              <p class="legal">
                By providing your email address you agree to receive digital communications from Firma Foreign Exchange.
                You may unsubscribe <span class="nobr">at anytime.</span>
              </p>
            </form>
            <div class="complete-message">
              <h1>Nice to meet you.</h1>
              <p>
                We&rsquo;ll be in touch shortly.
                <span class="nobr">Thank you.</span>
              </p>
            </div>
          </div>

        </div>
      </section>

    </div>
  </div>

</div>

</asp:content>