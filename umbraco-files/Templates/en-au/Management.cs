<%@ Master Language="C#" MasterPageFile="~/masterpages/en-au.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/" class="brand-link">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="234.4px" height="42.1px" viewBox="0 0 234.4 42.1" enable-background="new 0 0 234.4 42.1" preserveAspectRatio="xMinYMin meet"
                     shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo">
                    <polygon points="34.8,0 34.8,39.2 42.5,39.2 42.5,0 "/>
                    <path d="M73.9,23c7.5-4.1,7.2-11.9,7.2-11.9c0-10.9-11.9-11-11.9-11H49.6v39.1h7.6l0-14.7h9.2l8,14.7l8.9,0L73.9,23z
                       M73.6,12c0.1,5.1-6,5-6,5H57.4V7.6h11C74.1,7.7,73.6,12,73.6,12"/>
                    <path d="M27.6,7.6V0.1h-20C3.6,0.1,0.4,2.6,0,5.9c0,0.2,0,33.3,0,33.3h7.7l0-15.5l18.1,0V16l-18,0l0-8.4H27.6z"/>
                    <path d="M127.4,4.6c-0.1-2.5-2.2-4.6-4.7-4.6c-1.8,0-3.3,1-4.1,2.4l0,0l-10.6,18.8c0,0-10.5-18.4-10.5-18.6
                      C96.3,1,95,0,93.1,0c-2.6,0-4.7,2.1-4.7,4.7c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1v34.3H96l0-24.5l8.9,16.7h6l8.8-16.7v24.5h7.7
                      C127.4,39.3,127.4,4.7,127.4,4.6"/>
                    <path d="M155.5,2.5c-0.8-1.4-2.3-2.4-4-2.4c-1.8,0-3.3,1-4.1,2.5l-14.9,36.7h8.5l3.7-9.5h13.4l3.8,9.5h8.5L155.5,2.5
                      z M147.2,23.4l4.2-12.8l4.3,12.8H147.2z"/>
                    <polygon points="176.5,25.9 176.5,16.8 182,16.8 182,18.3 178.1,18.3 178.1,20.5 181.7,20.5 181.7,22 178.1,22
                      178.1,25.9 "/>
                    <path d="M182.7,22.6c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1c1,0,1.8,0.3,2.5,1c0.7,0.7,1,1.5,1,2.5
                      c0,1-0.4,1.8-1,2.5c-0.7,0.7-1.5,1-2.5,1c-1,0-1.8-0.3-2.5-1C183.1,24.4,182.7,23.6,182.7,22.6 M187.8,24.1c0.4-0.4,0.6-0.9,0.6-1.5
                      s-0.2-1.1-0.6-1.5c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1.1,0.2-1.5,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.5,0.6C186.9,24.6,187.4,24.5,187.8,24.1"/>
                    <path d="M191,25.9v-6.6h1.5v1c0.3-0.7,1-1.1,2-1.1c0.2,0,0.3,0,0.5,0v1.5c-0.2,0-0.4-0.1-0.7-0.1
                      c-1.1,0-1.8,0.7-1.8,1.8v3.5H191z"/>
                    <path d="M195.6,22.6c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C195.9,24.5,195.6,23.7,195.6,22.6L195.6,22.6z M200.7,21.9c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H200.7z"/>
                    <path d="M203.2,17.6c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
                      C203.6,18.5,203.2,18.1,203.2,17.6 M203.3,19.3h1.6v6.6h-1.6V19.3z"/>
                    <path d="M211.4,20.3v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7V25c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C210.3,19.2,211.1,19.6,211.4,20.3 M210.9,24
                      c0.4-0.4,0.6-0.9,0.6-1.4s-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4s0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C210,24.5,210.5,24.4,210.9,24"/>
                    <path d="M214.4,25.9v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1h-1.6V22c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H214.4z"/>
                    <polygon points="176.5,39.2 176.5,30.2 182.3,30.2 182.3,31.6 178.1,31.6 178.1,33.8 182,33.8 182,35.3 178.1,35.3
                      178.1,37.8 182.4,37.8 182.4,39.2 "/>
                    <polygon points="183.2,39.2 185.7,35.8 183.4,32.7 185.1,32.7 186.6,34.6 188,32.7 189.7,32.7 187.4,35.8
                      189.9,39.2 188.2,39.2 186.6,36.9 184.9,39.2 "/>
                    <path d="M196.3,33.6l-1,1c-0.5-0.5-1-0.7-1.7-0.7c-0.6,0-1.1,0.2-1.4,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.4,0.6c0.7,0,1.2-0.2,1.7-0.7l1,1c-0.7,0.8-1.6,1.1-2.6,1.1c-1,0-1.8-0.3-2.5-1c-0.7-0.7-1-1.5-1-2.5
                      s0.3-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1C194.7,32.5,195.6,32.9,196.3,33.6"/>
                    <path d="M197.2,39.2v-9.1h1.5v3.4c0.3-0.6,1-1,1.9-1c1.5,0,2.4,1,2.4,2.6v4.1h-1.6v-3.8c0-0.9-0.5-1.5-1.3-1.5
                      c-0.8,0-1.4,0.6-1.4,1.5v3.8H197.2z"/>
                    <path d="M209.5,33.6v-1h1.6v6.6h-1.6v-1c-0.2,0.6-1,1.1-2.1,1.1c-0.9,0-1.7-0.3-2.3-1c-0.6-0.7-0.9-1.5-0.9-2.4
                      s0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C208.5,32.5,209.3,33,209.5,33.6 M209,37.4c0.4-0.4,0.6-0.9,0.6-1.4c0-0.6-0.2-1.1-0.6-1.4
                      c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.9-0.5,1.4c0,0.6,0.2,1.1,0.5,1.4c0.4,0.4,0.8,0.6,1.4,0.6
                      C208.2,38,208.7,37.8,209,37.4"/>
                    <path d="M212.5,39.2v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1H217v-3.8c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H212.5z"/>
                    <path d="M225,33.6v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7v-0.3c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,1-2.4c0.6-0.7,1.4-1,2.3-1C224,32.5,224.8,33,225,33.6 M224.5,37.4c0.4-0.4,0.6-0.9,0.6-1.4
                      c0-0.6-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4c0,0.6,0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C223.7,37.9,224.2,37.7,224.5,37.4"/>
                    <path d="M227.7,36c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C228,37.9,227.7,37,227.7,36L227.7,36z M232.8,35.3c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H232.8z"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/services/">
                Our Services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link active" href="/company/">
                Our Company
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/team/" class="secondary-nav-link active">Management Team</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/compliance/" class="secondary-nav-link">Compliance <span class="soft-break"></span>and
                    Accreditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Careers</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/blog/">
                Blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Australia
              </a>

              <div class="dropdown-menu">

                <a class="dropdown-item" href="https://firmafx.ca/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.ca/fr-ca/">Canada <span>FR</span></a>

                <a class="dropdown-item" href="https://firmafx.co.nz">New Zealand</a>

                <a class="dropdown-item" href="https://firmafx.co.uk">United Kingdom</a>

                <a class="dropdown-item" href="https://firmafx.com/en-us/">United States</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <div class="main-content container">

      <div class="row-sidebar">
        <div class="sidebar"></div>
        <div class="page-content">

          <section class="hero hero-secondary" style="background-image:url(/images/company/background-management.jpg)" ;>
            <div class="contents container">
              <div class="text">
                <div class="firma-equals">
                  <img src="/images/text-firma-equels.svg" class="text-firma-equals">
                  <div class="handwriting">
                    <div class="sprite engagement"></div>

                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="container light">
            <h1>Meet our <span class="soft-break mobile"></span> management team.</h1>
            <ul class="team-list accordion">

              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Adam Wanless</span>
                    <span class="job-title">General Manager</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Adam Wanless is the General Manager Australian Operations. Adam brings over 20 years of experience in
                    the Telecommunications, Commodity Exchange and Franchising Industries to FIRMA Foreign Exchange. Prior
                    to joining Firma in 2016, he held positions on increasing responsibility with the world’s largest trade
                    exchange Barteracrd Ltd. Over his 5+ years at Bartercard he held both national and international
                    management postings specifically within the Asia-pacific region.
                  </p>
                  <p>
                    As General Manager of Australian Operations, Adam's focus is to uncover new business opportunities and
                    to ensure the Firma brand continues to provide a best in class sales and service experience to all
                    clients. His experience and extensive knowledge of the Asia-pacific business landscape, coupled with his
                    strengths in leadership, talent development and program delivery, make him the ideal choice to lead our
                    Australian operations.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Ben Weekes</span>
                    <span class="job-title">National Assistant Manager</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Ben Weekes is an FX Specialist and National Assistant Manager. Since 2015 he has managed all aspects of
                    the trading floor, ensuring Firma’s customers receive market leading information and service in a timely
                    and accurate manner allowing them to make informed decisions for their business.
                  </p>
                  <p>
                    Ben’s career spans a number of industries, including agriculture, namely beef and dairy where he
                    continues to be involved in his family’s business on the mid-north coast of NSW.
                  </p>
                  <p>
                    He also served in the Australian Defence Force as a paratrooper with the 3rd Battalion, serving with
                    distinction in East Timor and gaining valuable leadership experience.
                  </p>
                  <p>
                    As the Responsible Manager for Firma’s financial licence, Ben maintains the Australian operation’s
                    superb compliance record. Ben holds a Bachelor of Science Degree and continues to stay abreast of
                    developments and changes in anti-money laundering and counter terrorism financing.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Dave Dominy</span>
                    <span class="job-title">Chief Executive Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Dave Dominy joined Firma in June 2014 as CEO, bringing significant financial industry expertise and
                    experience to the company. He spent 15 years in a variety of senior banking and investment banking roles
                    with a Canadian chartered bank, as well as 8 years as CEO of a private equity group in Western Canada.
                    As CEO of a provincial regulatory body, he was directly involved in the implementation of new liquidity
                    and capital standards for credit unions. From 1999 through 2003 he was CEO of a mid-sized public company
                    active in the oilfield services sector and, prior to that, was CFO of a large-cap public company active
                    in gas processing and transportation.
                  </p>
                  <p>
                    His extensive board career includes leadership roles on a variety of not-for-profit, private, public and
                    crown corporation boards. He is currently Vice Chair and director of Norquest College, Chair Audit
                    Committee and director of Chandos Construction LP, director of the Alberta Insurance Council, Chairman
                    of 3D Capital Inc. and an instructor in the Alberta School of Business’ governance program for small and
                    medium-sized enterprises.
                  </p>
                  <p>
                    He is an active contributor to a number of local charities, most recently as Chair of the Stollery
                    Children’s Hospital Foundation in Edmonton.
                  </p>
                  <p>
                    Dave holds a Masters of Management from the J.L.Kellogg School of Business in Chicago, a BA in
                    mathematics from the University of Winnipeg, the Canadian Securities Course and the ICD.D designation
                    from the Institute of Corporate Directors of Canada.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Michael Oshry</span>
                    <span class="job-title">Chairman</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    <img src="https://firmafx.com/media/40376/Edmonton-Made-Black-Transparent.png" width="100" hspace="0" vspace="0" align="right"/>
                    Michael began his career starting a variety of local businesses in retail, hospitality and business
                    services. He co-founded FIRMA Foreign Exchange in 1998 and co-won the Ernst & Young Entrepreneur of the
                    Year Award in 2007.
                  </p>
                  <p>
                    Michael was elected to represent Ward 5 on Edmonton City Council in October 2013. In that capacity he
                    has served on a variety of committees, boards and initiatives including the City Manager and City
                    Auditor Performance Evaluation, Chairing the Transportation Committee, Community Standards and Licensing
                    Appeal Board, Edmonton Northlands, the Emerging Economy Initiative and the Edmonton Police Commission.
                  </p>
                  <p>
                    He is also a past board member of the Alberta chapter of the Young Presidents' Organization (YPO) and
                    served on the boards of other non-profit organizations.
                  </p>
                  <p>
                    Michael is married and the proud father of two children.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">James Devenish</span>
                    <span class="job-title">Chief Sales Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    James brings over 20 years of experience in the Financial Industry to the Firma Foreign Exchange team.
                    Prior to his joining Firma in 2011, he held positions with both Western Union Business Solutions and
                    HSBC Financial Canada, where his operations and sales skills were honed through both national and
                    international client relationships.
                  </p>
                  <p>
                    Since joining Firma, James has consistently demonstrated his passion for delivering bottom line results
                    by focusing on the development of high performing teams, sales management and strategic planning. His
                    success is well demonstrated with numerous sales, business growth and profitability achievements
                    throughout his career.
                  </p>
                  <p>
                    As Executive Vice President and Chief Sales Officer, James’ focus is to uncover new business
                    opportunities and to ensure the global business continues to provide a best in class sales and service
                    experience to all clients. His experience and extensive knowledge of the industry, coupled with his
                    strengths in leadership, talent development and program delivery, make him the ideal choice to lead
                    Firma's global sales teams.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Alex Eadie, CPA, CMA, CAMS</span>
                    <span class="job-title">Chief Financial Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Alex Eadie is a certified management accountant (CMA) and certified anti-money laundering specialist
                    (CAMS) who brings 12 years of executive experience to the FIRMA Foreign Exchange team. Most recently, as
                    chief accountant and general manager, Alex led the rapid growth of a leading edge software integration
                    firm.
                  </p>
                  <p>
                    To enhance the company’s competitive edge, Alex brings his professional yet creative blend of promoting
                    sound business fundamentals with leading edge management techniques. Alex’s financial acumen, strategic
                    insight and collaborative approach to implementing a variety of initiatives are instrumental in FIRMA
                    Foreign Exchange’s ongoing international growth.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Brad Clark, BComm, BSc</span>
                    <span class="job-title">Chief Operating Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Brad Clark comes to FIRMA Foreign Exchange with over fifteen years of Operations Management experience.
                    He holds a Bachelor of Commerce degree from the University of Alberta and a Bachelor of Science degree
                    in mathematics. Brad’s communication and leadership skills, along with his ability to work well under
                    pressure, have contributed in large part to his success.
                  </p>
                  <p>
                    Prior to his position with Firma, Brad was the general manager of Western Canada’s largest commercial
                    print manufacturer, a division of one of the largest international print media corporations. While
                    there, Brad was identified as one of the top 75 high-potential employees from across North America and
                    received training and career development in a Leadership, Excellence and Development Program.
                  </p>
                  <p>
                    As Chief Operating Officer, Brad coordinates and grows a diverse team of professionals in information
                    technology services, communications and trading personnel. Brad brings the dedication and perseverance
                    required to strategically grow the business and support the critical operations component for the
                    continued success of FIRMA Foreign Exchange.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Chris Brockbank</span>
                    <span class="job-title">Chief Marketing Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Chris brings nearly 30 years of marketing experience to the FIRMA Foreign Exchange team. Chris’ previous
                    experience includes chief marketing officer at Sears Canada, Moneris, Just Energy and Sun/Quebecor
                    Media. In addition, Chris also held senior marketing roles at Sirius Canada, Bell/Bell Wireless and
                    Nortel Networks.
                  </p>
                  <p>
                    In addition to Chris’ vast wealth of knowledge and experience, he is also an instructor at University of
                    Toronto where he teaches brand management in the continuing education program.
                  </p>
                  <p>
                    As Firma’s chief marketing officer, Chris will be researching, implementing and uncovering ways to take
                    Firma’s brand, products and services to the next level while increasing Firma's competitive edge in the
                    marketplace to propel Firma to additional international growth.
                  </p>
                </div>
              </li>
              <li class="team-item accordion-item">
                <h3 class="member-heading accordion-heading">
                  <a class="member-link accordion-link " href="#">
                    <span class="name">Gordon Wollenberg, BCom, CHRP</span>
                    <span class="job-title">Chief Talent Officer</span>
                  </a>
                </h3>
                <div class="description accordion-contents collapse">
                  <p>
                    Gordon Wollenberg joined Firma in March 2013 with over 20 years of human resources experience. He holds
                    a Bachelor of Commerce degree from the University of Alberta and is a Certified Human Resources
                    Professional, holding his designation with the HRIA.
                  </p>
                  <p>
                    Prior to joining Firma, Gordon was the human resources manager of a large Canadian construction company.
                    Previous experiences were with City of Edmonton, Government of Alberta, ATB and the Catholic School
                    Board.
                  </p>
                  <p>
                    As chief talent officer, Gordon is responsible for the overall operation of the human resources function
                    of Firma. Gordon and his team focus on the recruitment of high-caliber individuals, as well as learning
                    and development initiatives to ensure high performance and continued success for Firma.
                  </p>
                </div>
              </li>
            </ul>
          </section>

        </div>
      </div>

    </div>
	</div>
</asp:content>