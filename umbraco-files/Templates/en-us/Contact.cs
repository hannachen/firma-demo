<%@ Master Language="C#" MasterPageFile="~/masterpages/en-us.master" AutoEventWireup="true" %>

<asp:content ContentPlaceHolderId="RootContentPlaceholder" runat="server">
	<div class="interior nosidebar">

    <div id="header">
      <input type="hidden" autofocus="true"/>
      <div class="container navbar">
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav">
          <span class="hamburger"><span class="icon icon-mobile-menu"></span></span>
          <span class="close"><span class="icon icon-firma-close"></span></span>
        </button>
        <h3 class="logo text-muted navbar-brand">
          <a href="/en-us/" class="brand-link">

            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     width="234.4px" height="42.1px" viewBox="0 0 234.4 42.1" enable-background="new 0 0 234.4 42.1" preserveAspectRatio="xMinYMin meet"
                     shape-rendering="geometricPrecision" xml:space="preserve" class="nav-logo">
                    <polygon points="34.8,0 34.8,39.2 42.5,39.2 42.5,0 "/>
                    <path d="M73.9,23c7.5-4.1,7.2-11.9,7.2-11.9c0-10.9-11.9-11-11.9-11H49.6v39.1h7.6l0-14.7h9.2l8,14.7l8.9,0L73.9,23z
                       M73.6,12c0.1,5.1-6,5-6,5H57.4V7.6h11C74.1,7.7,73.6,12,73.6,12"/>
                    <path d="M27.6,7.6V0.1h-20C3.6,0.1,0.4,2.6,0,5.9c0,0.2,0,33.3,0,33.3h7.7l0-15.5l18.1,0V16l-18,0l0-8.4H27.6z"/>
                    <path d="M127.4,4.6c-0.1-2.5-2.2-4.6-4.7-4.6c-1.8,0-3.3,1-4.1,2.4l0,0l-10.6,18.8c0,0-10.5-18.4-10.5-18.6
                      C96.3,1,95,0,93.1,0c-2.6,0-4.7,2.1-4.7,4.7c0,0,0,0.1,0,0.1c0,0,0,0.1,0,0.1v34.3H96l0-24.5l8.9,16.7h6l8.8-16.7v24.5h7.7
                      C127.4,39.3,127.4,4.7,127.4,4.6"/>
                    <path d="M155.5,2.5c-0.8-1.4-2.3-2.4-4-2.4c-1.8,0-3.3,1-4.1,2.5l-14.9,36.7h8.5l3.7-9.5h13.4l3.8,9.5h8.5L155.5,2.5
                      z M147.2,23.4l4.2-12.8l4.3,12.8H147.2z"/>
                    <polygon points="176.5,25.9 176.5,16.8 182,16.8 182,18.3 178.1,18.3 178.1,20.5 181.7,20.5 181.7,22 178.1,22
                      178.1,25.9 "/>
                    <path d="M182.7,22.6c0-1,0.4-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1c1,0,1.8,0.3,2.5,1c0.7,0.7,1,1.5,1,2.5
                      c0,1-0.4,1.8-1,2.5c-0.7,0.7-1.5,1-2.5,1c-1,0-1.8-0.3-2.5-1C183.1,24.4,182.7,23.6,182.7,22.6 M187.8,24.1c0.4-0.4,0.6-0.9,0.6-1.5
                      s-0.2-1.1-0.6-1.5c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1.1,0.2-1.5,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.5,0.6C186.9,24.6,187.4,24.5,187.8,24.1"/>
                    <path d="M191,25.9v-6.6h1.5v1c0.3-0.7,1-1.1,2-1.1c0.2,0,0.3,0,0.5,0v1.5c-0.2,0-0.4-0.1-0.7-0.1
                      c-1.1,0-1.8,0.7-1.8,1.8v3.5H191z"/>
                    <path d="M195.6,22.6c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C195.9,24.5,195.6,23.7,195.6,22.6L195.6,22.6z M200.7,21.9c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H200.7z"/>
                    <path d="M203.2,17.6c0-0.5,0.4-0.9,0.9-0.9c0.5,0,0.9,0.4,0.9,0.9c0,0.5-0.4,0.9-0.9,0.9
                      C203.6,18.5,203.2,18.1,203.2,17.6 M203.3,19.3h1.6v6.6h-1.6V19.3z"/>
                    <path d="M211.4,20.3v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7V25c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C210.3,19.2,211.1,19.6,211.4,20.3 M210.9,24
                      c0.4-0.4,0.6-0.9,0.6-1.4s-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4s0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C210,24.5,210.5,24.4,210.9,24"/>
                    <path d="M214.4,25.9v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1h-1.6V22c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H214.4z"/>
                    <polygon points="176.5,39.2 176.5,30.2 182.3,30.2 182.3,31.6 178.1,31.6 178.1,33.8 182,33.8 182,35.3 178.1,35.3
                      178.1,37.8 182.4,37.8 182.4,39.2 "/>
                    <polygon points="183.2,39.2 185.7,35.8 183.4,32.7 185.1,32.7 186.6,34.6 188,32.7 189.7,32.7 187.4,35.8
                      189.9,39.2 188.2,39.2 186.6,36.9 184.9,39.2 "/>
                    <path d="M196.3,33.6l-1,1c-0.5-0.5-1-0.7-1.7-0.7c-0.6,0-1.1,0.2-1.4,0.6c-0.4,0.4-0.6,0.9-0.6,1.5s0.2,1.1,0.6,1.5
                      c0.4,0.4,0.9,0.6,1.4,0.6c0.7,0,1.2-0.2,1.7-0.7l1,1c-0.7,0.8-1.6,1.1-2.6,1.1c-1,0-1.8-0.3-2.5-1c-0.7-0.7-1-1.5-1-2.5
                      s0.3-1.8,1-2.5c0.7-0.7,1.5-1,2.5-1C194.7,32.5,195.6,32.9,196.3,33.6"/>
                    <path d="M197.2,39.2v-9.1h1.5v3.4c0.3-0.6,1-1,1.9-1c1.5,0,2.4,1,2.4,2.6v4.1h-1.6v-3.8c0-0.9-0.5-1.5-1.3-1.5
                      c-0.8,0-1.4,0.6-1.4,1.5v3.8H197.2z"/>
                    <path d="M209.5,33.6v-1h1.6v6.6h-1.6v-1c-0.2,0.6-1,1.1-2.1,1.1c-0.9,0-1.7-0.3-2.3-1c-0.6-0.7-0.9-1.5-0.9-2.4
                      s0.3-1.8,0.9-2.4c0.6-0.7,1.4-1,2.3-1C208.5,32.5,209.3,33,209.5,33.6 M209,37.4c0.4-0.4,0.6-0.9,0.6-1.4c0-0.6-0.2-1.1-0.6-1.4
                      c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.9-0.5,1.4c0,0.6,0.2,1.1,0.5,1.4c0.4,0.4,0.8,0.6,1.4,0.6
                      C208.2,38,208.7,37.8,209,37.4"/>
                    <path d="M212.5,39.2v-6.6h1.5v0.9c0.3-0.6,1.1-1.1,2-1.1c1.6,0,2.5,1,2.5,2.6v4.1H217v-3.8c0-0.9-0.5-1.5-1.4-1.5
                      c-0.9,0-1.5,0.6-1.5,1.5v3.8H212.5z"/>
                    <path d="M225,33.6v-1h1.6v5.5c0,1.6-0.2,2.5-1.1,3.2c-0.6,0.5-1.4,0.7-2.3,0.7c-1.2,0-2.3-0.3-3.2-1l0.8-1.2
                      c0.7,0.5,1.5,0.7,2.3,0.7c0.6,0,1.1-0.1,1.4-0.4c0.4-0.3,0.6-0.9,0.6-1.7v-0.3c-0.3,0.6-1.1,1-2.1,1c-0.9,0-1.7-0.3-2.3-1
                      c-0.6-0.6-0.9-1.4-0.9-2.4c0-1,0.3-1.8,1-2.4c0.6-0.7,1.4-1,2.3-1C224,32.5,224.8,33,225,33.6 M224.5,37.4c0.4-0.4,0.6-0.9,0.6-1.4
                      c0-0.6-0.2-1-0.6-1.4c-0.4-0.4-0.9-0.6-1.4-0.6c-0.6,0-1,0.2-1.4,0.6c-0.4,0.4-0.5,0.8-0.5,1.4c0,0.6,0.2,1,0.5,1.4
                      c0.4,0.4,0.8,0.6,1.4,0.6C223.7,37.9,224.2,37.7,224.5,37.4"/>
                    <path d="M227.7,36c0-1,0.3-1.9,1-2.5c0.7-0.6,1.5-1,2.5-1c1,0,1.7,0.3,2.3,1c0.6,0.6,0.9,1.4,0.9,2.4
                      c0,0.2,0,0.4,0,0.5h-5.1c0.1,1,0.8,1.7,2,1.7c0.7,0,1.3-0.2,1.8-0.8l0.9,0.9c-0.7,0.8-1.7,1.2-2.8,1.2c-1,0-1.9-0.3-2.5-0.9
                      C228,37.9,227.7,37,227.7,36L227.7,36z M232.8,35.3c0-0.4-0.2-0.7-0.5-1c-0.3-0.3-0.7-0.4-1.2-0.4c-0.5,0-0.9,0.1-1.3,0.4
                      c-0.4,0.3-0.5,0.6-0.6,1H232.8z"/>
                  </svg>
          </a>
        </h3>
        <nav id="main-nav" class="main-nav collapse navbar-toggleable-sm" role="navigation">
          <ul class="nav navbar-nav">
            <li class="spacer"></li>
            <li class="nav-item">
              <a class="nav-link" href="/en-us/services/">
                Our Services
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item our-company">
              <a class="nav-link" href="/en-us/company/">
                Our Company
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
              <ul class="nav-secondary nav-interior" data-toggle="position" data-toggle-on="desktop" data-position="after" data-parent=".our-company" data-container=".sidebar">
                <li class="secondary-nav-item">
                  <a href="/en-us/team/" class="secondary-nav-link">Management Team</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/en-us/compliance/" class="secondary-nav-link">Compliance <span class="soft-break"></span>and
                    Accreditations</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="https://careers.firmafx.com" class="secondary-nav-link">Careers</a>
                </li>
                <li class="secondary-nav-item">
                  <a href="/en-us/faq/" class="secondary-nav-link">FAQs</a>
                </li>
              </ul>
            </li>
            <li class="nav-item blog">
              <a class="nav-link" href="/en-us/blog/">
                Blog
                <i class="icon icon-chevron-right" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item dropdown language-selector">

              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                United States
              </a>
              <div class="dropdown-menu">

                <a class="dropdown-item" href="https://firmafx.ca/en-ca/">Canada <span>EN</span></a>

                <a class="dropdown-item" href="https://firmafx.ca/fr-ca/">Canada <span>FR</span></a>

                <a class="dropdown-item" href="https://firmafx.com.au">Australia</a>

                <a class="dropdown-item" href="https://firmafx.co.nz">New Zealand</a>

                <a class="dropdown-item" href="https://firmafx.co.uk">United Kingdom</a>

              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="main-content container">

      <div class="keyline"></div>
      <div class="page-content">

        <section class="container light">

          <h1>Contact Us</h1>

          <form runat="server">

            <asp:ScriptManager runat="server">
              <Scripts>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
              </Scripts>
            </asp:ScriptManager>
            <!--
            <umbraco:Macro Alias="ContactForm" runat="server"
                           CountryFilter="[#websiteLocationID]"
                           EmailFrom="[#emailFrom]"
                           EmailSupportTo="[#emailFormTo]"
                           EmailWebmaster="[#emailWebmaster]"
                           EmailCC="[#emailFormCC]"
                           SupportSubject="[#supportSubject]"
                           SupportContent="[#supportContent]"
                           SalesSubject="[#salesSubject]"
                           SalesContent="[#salesContent]"
                           ProblemSubject="[#problemSubject]"
                           ProblemContent="[#problemContent]"
                           RedirectID="[#redirectID]" />
                           -->
          </form>
          <div class="contact-row">
            <div class="col office-contact-container">
              <h2>
                Address and Phone
              </h2>

              <div class="office-contact">
                <ul class="contact-info">


                  <li class="location collapse" id="AZ0">
                    <h3 class="office-name">


                      Arizona

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (866) 426-2605</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (866) 426-5920</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="CA0">
                    <h3 class="office-name">


                      California

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (866) 822-8818</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (877) 682-8857</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="CT0">
                    <h3 class="office-name">


                      Connecticut

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="KS0">
                    <h3 class="office-name">


                      Kansas

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="MA0">
                    <h3 class="office-name">


                      Massachusetts

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="MN0">
                    <h3 class="office-name">


                      Minnesota

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (866) 262-2361</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (866) 262-5111</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="MT0">
                    <h3 class="office-name">


                      Montana

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (866) 426-2605</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (866) 426-5920</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="NJ0">
                    <h3 class="office-name">


                      New Jersey

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="NY0">
                    <h3 class="office-name">


                      New York

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse in" id="PA0">
                    <h3 class="office-name">


                      Philadelphia

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>

                      <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <span itemprop="streetAddress">802, 1760 Market Street</span><br/>
                        <span itemprop="addressLocality">Philadelphia, </span>
                        <span itemprop="addressRegion">Pennsylvania</span>
                        <span itemprop="postalCode">19103</span>
                        <br/><span itemprop="addressCountry">USA</span>
                      </address>

                      <div class="contact-details">
                        <abbr title="Toll-Free Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>
                        (toll free)

                        <br/>
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (267) 592-4770</span>


                        <br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="RI0">
                    <h3 class="office-name">


                      Rhode Island

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="SC0">
                    <h3 class="office-name">


                      South Carolina

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="TN0">
                    <h3 class="office-name">


                      Tennessee

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (855) 520-7819</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (855) 520-7820</span><br/>


                      </div>

                    </div>
                  </li>


                  <li class="location collapse" id="WI0">
                    <h3 class="office-name">


                      Wisconsin

                    </h3>

                    <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                      <div class="office-name">
                        <span itemprop="name">FIRMA Foreign Exchange Corporation</span>&nbsp;
                      </div>


                      <div class="contact-details">
                        <abbr title="Telephone">T:</abbr><!--
                                        --><span itemprop="telephone">1 (866) 445-6239</span>


                        <br/>

                        <abbr title="Fax">F:</abbr><!--
                                        --><span itemprop="faxNumber">1 (866) 502-3310</span><br/>


                      </div>

                    </div>
                  </li>

                </ul>
              </div>

              <button class="btn btn-default dropdown-toggle btn-block" id="office-dropdown" type="button" data-toggle="dropdown" aria-expanded="true">
                Additional Locations
                <span class="caret"></span>
              </button>

              <ul class="office-contact-list dropdown-menu" role="menu" aria-labelledby="office-dropdown">

                <li role="presentation" class="dropdown-header location-item">Arizona</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#AZ0">


                    Arizona

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">California</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#CA0">


                    California

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Connecticut</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#CT0">


                    Connecticut

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Kansas</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#KS0">


                    Kansas

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Massachusetts</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#MA0">


                    Massachusetts

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Minnesota</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#MN0">


                    Minnesota

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Montana</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#MT0">


                    Montana

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">New Jersey</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NJ0">


                    New Jersey

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">New York</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#NY0">


                    New York

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Pennsylvania</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#PA0">


                    Philadelphia

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Rhode Island</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#RI0">


                    Rhode Island

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">South Carolina</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#SC0">


                    South Carolina

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Tennessee</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#TN0">


                    Tennessee

                  </a>
                </li>


                <li role="presentation" class="dropdown-header location-item">Wisconsin</li>


                <li role="presentation" class="location-item city">
                  <a role="menuitem" tabindex="-1" class="office-contact-link" href="#WI0">


                    Wisconsin

                  </a>
                </li>


              </ul>
            </div>
            <div class="col contact-form-container">
              <h2>Learn how Firma can help <span class="nobr">your business.</span></h2>
              <form action="//go.pardot.com/l/108482/2016-09-16/cd5zc?" method="post" class="contact-form" id="contact-page-form" data-parsley-trigger="keyup blur">
                <div class="field first">
                  <input name="first" type="text" class="textinput" autocomplete="off" placeholder="First name" maxlength="20"
                         data-parsley-length="[2, 20]" data-parsley-length-message="Name should be between 2 and 20 characters" required=""/>
                </div>
                <div class="field email">
                  <input name="email" type="email" class="textinput" autocomplete="off" placeholder="Your email address" maxlength="40"
                         data-parsley-type-message="Invalid email address" required=""/>
                </div>
                <div class="button">
                  <button type="submit" class="submit" disabled>Submit</button>
                </div>
                <p class="legal">
                  By providing your email address you agree to receive digital communications from Firma Foreign Exchange.
                  You may unsubscribe <span class="nobr">at anytime.</span>
                </p>
              </form>
              <div class="complete-message">
                <h1>Nice to meet you.</h1>
                <p>
                  We&rsquo;ll be in touch shortly.
                  <span class="nobr">Thank you.</span>
                </p>
              </div>
            </div>
          </div>

          <div class="intro">
            <h1>Customer Feedback</h1>
            <p>
              FIRMA Foreign Exchange Corporation (U.S.) Ltd. is committed to working with you to find a fair and equitable
              resolution to any concerns that you may have about our products or services. We ask that your first contact
              regarding questions or concerns be with your trader at FIRMA Foreign Exchange. If you feel that we have not
              found a suitable resolution at this level, please contact us at:
            </p>


            <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

              <div class="office-name">
                <span itemprop="name">FIRMA Foreign Exchange Corporation</span>Compliance Department
              </div>

              <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="streetAddress">Edmonton City Centre East, Suite 400, 10205 101 Street</span><br/>
                <span itemprop="addressLocality">Edmonton, </span>
                <span itemprop="addressRegion">AB</span>
                <span itemprop="postalCode">T5J 4H5</span>

              </address>

              <div class="contact-details">
                <abbr title="Telephone">T:</abbr><!--
                            --><span itemprop="telephone">0011 1 (877) 376-4946</span>


                <br/>


                <span itemprop="email">customerfeedback@firmafx.com</span><br/>

              </div>

            </div>


          </div>

          <div class="intro">
            <p>If you feel that FIRMA Foreign Exchange Corporation (U.S.) Ltd. has not met your concerns in a fair and
              equitable manner, you may refer your concerns to the Consumer Financial Protection Bureau at:</p>


            <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

              <div class="office-name">
                <span itemprop="name">Consumer Financial Protection Bureau</span>&nbsp;
              </div>


              <div class="contact-details">
                <abbr title="Telephone">T:</abbr><!--
                            --><span itemprop="telephone">(855) 411-2372</span>


                <br/>
                <abbr title="Telephone">T:</abbr><!--
                            --><span itemprop="telephone">(855) 729-2372 (TTY/TDD)</span>


                <br/>


                <span itemprop="website">
                            <a href="http://www.consumerfinance.gov" target="_blank">www.consumerfinance.gov</a>
                          </span>
              </div>

            </div>


          </div>

          <div class="intro additional-contacts">
            <p>Alternatively you may provide feedback about FIRMA Foreign Exchange Corporation (U.S.) Ltd. to one of the
              individual state regulators using the state-specific contacts below:</p>


            <div class="office">


              <strong class="city-name">Arizona</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Arizona Department of Financial Institutions</span>&nbsp;
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">2910 N. 44th Street, Suite 310</span><br/>
                  <span itemprop="addressLocality">Phoenix</span>

                  <span itemprop="postalCode">85018</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(602) 771-2800</span>


                  <br/>


                  <span itemprop="email">customerfeedback@firmafx.com</span><br/>

                  <span itemprop="website">
                                  <a href="http://www.azdfi.gov" target="_blank">www.azdfi.gov</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">California</strong>

              <p>If you have complaints with respect to any aspect of the money transmission activities conducted at this
                location, you may contact:</p>

              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Consumer Services</span>Department of Business Oversight
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">1515 K Street, Suite 200</span><br/>
                  <span itemprop="addressLocality">Sacramento</span>

                  <span itemprop="postalCode">95814</span>

                </address>

                <div class="contact-details">
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">1-866-275-2677</span>
                  (toll free)

                  <br/>


                  <span itemprop="email">consumer.services@dbo.ca.gov</span><br/>

                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Connecticut</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Connecticut Department of Banking</span>Government Relations and Consumer
                  Affairs
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">260 Constitution Plaza</span><br/>
                  <span itemprop="addressLocality">Hartford</span>

                  <span itemprop="postalCode">06103-1800</span>

                </address>

                <div class="contact-details">
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(800) 831-7225</span>
                  (toll free)

                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.ct.gov.com" target="_blank">www.ct.gov.com</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Kansas</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Office of the State Bank Commissioner</span>&nbsp;
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">700 Jackson, Suite 300</span><br/>
                  <span itemprop="addressLocality">Topeka</span>

                  <span itemprop="postalCode">66603</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(785) 296-2266</span>


                  <br/>
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">800-622-0620</span>
                  (toll free)

                  <br/>


                  <span itemprop="email">complaints@osbckansas.org</span><br/>

                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Massachusetts</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Consumer Assistance Office</span>Massachusetts Division of Banks
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">1000 Washington Street, 10th floor</span><br/>
                  <span itemprop="addressLocality">Boston</span>

                  <span itemprop="postalCode">02118-2218</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(800) 495-2265 ext.501 within Massachusetts</span>


                  <br/>
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(617) 956-1500 ext. 501 outside of Massachusetts</span>


                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.mass.gov/ocabr" target="_blank">www.mass.gov/ocabr</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Minnesota</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">State of Minnesota Department of Commerce</span>Division of Financial
                  Institutions
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">85 7th Place East, Suite 500</span><br/>
                  <span itemprop="addressLocality">St. Paul</span>

                  <span itemprop="postalCode">55101-2198</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(651) 296-2135</span>


                  <br/>


                  <span itemprop="website">
                                  <a href="http://mn.gov/commerce/banking-and-finance" target="_blank">mn.gov/commerce/banking-and-finance</a>
                                </span>
                </div>

              </div>
              <p><strong>Voluntary Disqualification by Customer:</strong> As per Minnesota Statute 53B.27, subdivision 2,
                you are entitled to voluntary disqualify yourself from sending or receiving money transfers. This
                disqualification last for one year unless you give instructions that it be in effect for a period longer
                than one year. To apply for voluntary disqualification, please fill out the form linked here. You may
                terminate this disqualification at any time upon written request to FIRMA Foreign Exchange Corporation
                (U.S.) Ltd.</p>

            </div>
            <div class="office">


              <strong class="city-name">Montana</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">State of Montana Department of Administration</span>Division of Banking and
                  Financial Institutions
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">301 South Park, Suite 316 – PO Box 200546</span><br/>
                  <span itemprop="addressLocality">Helena</span>

                  <span itemprop="postalCode">59620-0546</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(406) 841-2918</span>


                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.banking.mt.gov" target="_blank">www.banking.mt.gov</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">New Jersey</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">New Jersey Department of Banking and Insurance</span>Consumer Inquiry and
                  Response Center (“CIRC”)
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">P.O. Box 471</span><br/>
                  <span itemprop="addressLocality">Trenton</span>

                  <span itemprop="postalCode">08625-0471</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(609) 292-7272</span>


                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.state.nj.us/dobi/consumer.htm" target="_blank">www.state.nj.us/dobi/consumer.htm</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">New York</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Department of Financial Services</span>Consumer Assistance Unit
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">One Commerce Plaza</span><br/>
                  <span itemprop="addressLocality">Albany</span>

                  <span itemprop="postalCode">12257</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">1 (212) 480-6400</span>


                  <br/>


                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Pennsylvania</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Pennsylvania Department of Banking</span>Consumer Services
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">17 N. Second Street, Suite 1300</span><br/>
                  <span itemprop="addressLocality">Harrisburg</span>

                  <span itemprop="postalCode">17101-2290</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(717) 787-1854</span>


                  <br/>
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(800)PA.Banks</span>
                  (toll free)

                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.banking.state.pa.us" target="_blank">www.banking.state.pa.us</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Rhode Island</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">State of Rhode Island Department of Business Regulation</span>Division of
                  Banking
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">1511 Pontiac Avenue, Bldg. 68-2</span><br/>
                  <span itemprop="addressLocality">Cranston</span>

                  <span itemprop="postalCode">02920</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">1 (401) 462-9500</span>


                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.dbr.ri.gov" target="_blank">www.dbr.ri.gov</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">South Carolina</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Consumer Complaint</span>S.C. Department of Consumer Affairs
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">P.O. Box 5757</span><br/>
                  <span itemprop="addressLocality">Columbia</span>

                  <span itemprop="postalCode">29250-5757</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(803) 734-4200</span>


                  <br/>
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(800) 922-1595</span>
                  (toll free)

                  <br/>


                  <span itemprop="email">scda@scconsumer.gov</span><br/>

                  <span itemprop="website">
                                  <a href="http://www.scconsumer.gov" target="_blank">www.scconsumer.gov</a>
                                </span>
                </div>

              </div>


            </div>
            <div class="office">


              <strong class="city-name">Wisconsin</strong>


              <div class="office-item-container" itemscope itemtype="http://schema.org/Organization">

                <div class="office-name">
                  <span itemprop="name">Department of Financial Institutions</span>Office of Consumer Affairs
                </div>

                <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                  <span itemprop="streetAddress">PO Box 8041</span><br/>
                  <span itemprop="addressLocality">Madison</span>

                  <span itemprop="postalCode">53708-8041</span>

                </address>

                <div class="contact-details">
                  <abbr title="Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(608) 264-7969</span>


                  <br/>
                  <abbr title="Toll-Free Telephone">T:</abbr><!--
                                  --><span itemprop="telephone">(800) 452-3328</span>
                  (toll free)

                  <br/>


                  <span itemprop="website">
                                  <a href="http://www.wdfi.org" target="_blank">www.wdfi.org</a>
                                </span>
                </div>

              </div>


            </div>

          </div>

        </section>

      </div>

    </div>
	</div>
</asp:content>