<%@ Master Language="C#" MasterPageFile="~/masterpages/Root.master" AutoEventWireup="true" CodeBehind="PageLayout-BasePage.master.cs" Inherits="FirmaFx.Web.Masterpages.PageLayout_BasePage" %>
<%@ Register Src="~/UserControls/SubMenu.ascx" TagPrefix="ui" TagName="SubMenu" %>
<%@ MasterType VirtualPath="~/Masterpages/Root.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="RootStylePlaceholder" runat="server">
    <asp:ContentPlaceHolder Id="StylePlaceholder" runat="server" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="RootAnalyticsPlaceholder" runat="server">
    <umbraco:Macro Alias="GoogleAnalytics" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="RootContentPlaceholder" runat="server">

    <umbraco:Macro Alias="LanguageSelector" runat="server" />

    <div class="container navigation">
        <umbraco:Macro Alias="TopMenu" runat="server" />
    </div>

    <div class="container header">
        <div class="row">
            <div class="col-md-12 img-row">
                <img src="/images/header_inside_ca.jpg" class="img-responsive" alt="" runat="server" id="imgHeader" />
            </div>
        </div>
        <div class="row" id="firma_message">
            <div class="col-md-12 hidden-xs hidden-sm">
                <umbraco:Item field="homeText" recursive="true" runat="server" />
            </div>
        </div>
    </div>

    <div class="container main">
        <div class="row">
            <div class="col-md-12">
                <umbraco:Macro Alias="Breadcrumb" runat="server" />
            </div>
        </div>
        <div class="row content">
            <div class="col-md-3 hidden-xs hidden-sm sidebar">

                <asp:Panel ID="pnlFirst" runat="server" CssClass="corporate_services">
                    <ui:SubMenu runat="server" ID="FirstMenu" />
                </asp:Panel>
                <asp:Panel ID="pnlSecond" runat="server" CssClass="private_client_services">
                    <ui:SubMenu runat="server" ID="SecondMenu" />
                </asp:Panel>
                <asp:Panel ID="pnlThird" runat="server" CssClass="company">
                    <ui:SubMenu runat="server" ID="ThirdMenu" />
                </asp:Panel>

            </div>
            <div class="col-md-9 col-sm-12">

                <asp:ContentPlaceHolder Id="ContentPlaceholder" runat="server" />

            </div>
        </div>
    </div>
    <div id="footer" class="container clearfix">
        <umbraco:Macro Alias="Footer" runat="server" />
    </div>

</asp:Content>